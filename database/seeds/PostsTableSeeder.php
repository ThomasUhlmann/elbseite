<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Elbsingles\Posts\Post;
use Elbsingles\Posts\Tags\Tag;

class PostsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		$tags = Tag::where('parent_id', '!=', '')->get(['id'])->fetch('id')->toArray();

		foreach(range(1, 50) as $index)
		{
			$post = Post::create([
				'body'	 		=> $faker->paragraph(3),
				'user_id'	  	=> $faker->numberBetween(1,40),
				'privateMessaging' => $faker->boolean(50),
				'created_at' 	=> $faker->dateTimeBetween('-30 days', 'now'),
			]);

			$post->tags()->sync( $faker->randomElements($tags, $faker->numberBetween(1,3)) );
		}
	}

}
