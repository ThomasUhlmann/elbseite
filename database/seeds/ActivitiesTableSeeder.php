<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Elbsingles\Activities\Activity;
use Elbsingles\Posts\Post;
use Elbsingles\Posts\Tags\Tag;

class ActivitiesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		$tags = Tag::where('parent_id', '<>', '')->get(['id'])->fetch('id')->toArray();

		foreach(range(1, 20) as $index)
		{
			$activity = Activity::create([
				'title'	  => $faker->sentence(3),
				'user_id' => $faker->numberBetween(1,40),
				'body'	  => $faker->paragraph(4),
				'privateMessaging' => $faker->boolean(50),
				'date'	  => $faker->dateTimeBetween('now', '+60 days'),
				'location'=> $faker->randomElement(['Beatpol', 'Scheune', 'Staatsschauspielhaus', 'Projekttheater', 'Nikkifaktur', 'Glücksgas-Stadion', 'Großer Garten']),
				'created_at'=>$faker->dateTimeBetween('-30 days', 'now'),
			]);

			$activity->tags()->sync( $faker->randomElements($tags, $faker->numberBetween(1, 3)) );
		}
	}

}
