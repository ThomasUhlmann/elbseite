<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Elbsingles\Posts\Tags\Tag;

class TagsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		$tags = [
			'Aktionen' => [
				'Disco',
				'Kochen',
				'Spieleabend'
			],
			'Hobbies' => [
				'Musik',
				'Tanzen',
			],
			'Kultur' => [
				'Kino',
				'Konzerte',
				'Literatur',
				'Museen',
				'Sprachen',
				'Theater',
			],
			'Sport' => [
				'Basketball',
				'Fußball',
				'Joggen',
				'Tischtennis',
				'Volleyball',
			],
			'Unterwegs' => [
				'Geocaching',
				'Radfahren',
				'Spazieren',
				'Wandern'
			],
		];

		foreach($tags as $parent => $subtags)
		{
			$main = Tag::create([
				'name'		=> $parent,
				'slug'		=> Str::slug($parent),
				'parent_id' => null
			]);

			foreach ($subtags as $subtag)
			{
				Tag::create([
					'name'		=> $subtag,
					'slug'		=> Str::slug($subtag),
					'parent_id' => $main->id
				]);
			}
		}
	}

}
