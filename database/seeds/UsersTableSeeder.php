<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Elbsingles\Users\User;
use Elbsingles\Users\Roles\Role;
use Elbsingles\Users\Profiles\Profile;
use Elbsingles\Users\Profiles\ProfileOptions;
use Elbsingles\Users\Locations\Location;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		$locations = [
			'Dresden',
			'Freital',
			'Meißen',
			'Freiberg',
			'Bautzen',
			'Radebeul',
			'Pirna',
			'Heidenau',
		];

		$faker = Faker::create();

		$thomas = User::create([
			'username'	=> 'Thomas',
			'email'		=> 'tho.uhlmann@gmail.com',
			'password'	=> '12345',
			'gender'	=> 'male',
			'dob'		=> '1984-12-11'
		]);

		$admin = Role::whereName('admin')->first();
		$thomas->assignRole($admin);

		$profile = $thomas->profile;
		$profile->fill([
			'user_id'	=> $thomas->id,
			'height'	=> 190,
			'build'		=> 2,
			'smoker'	=> 1,
			'children'	=> 4,
			'vegetarian'=> 2,
			'education' => 5,
			'about'		=> 'Bla Bla bla',
		])->save();
		
		$location = Location::firstOrCreate(['city' => 'Dresden']);
		$thomas->location()->associate($location);
		$thomas->save();

		$user = User::create([
			'username'	=> 'TestMan',
			'email'		=> 'man@test.com',
			'password'	=> '12345',
			'gender'	=> 'male',
			'dob'		=> '1976-06-06'
		]);

		$user->location()->associate($location);
		$user->save();

		$user = User::create([
			'username'	=> 'TestWoman',
			'email'		=> 'woman@test.com',
			'password'	=> '12345',
			'gender'	=> 'female',
			'dob'		=> '1987-09-08'
		]);

		$user->location()->associate($location);
		$user->save();
		
		$profile = new Profile;

		foreach(range(1, 40) as $index)
		{
			$user = User::create([
				'username'	=> $faker->userName,
				'email'		=> $faker->email,
				'password'	=> '12345',
				'gender'	=> $faker->randomElement(['male', 'female']),
				'dob'		=> $faker->dateTimeBetween('-60 years', '-18 years')->format('Y-m-d'),
				'last_active'=>$faker->dateTimeBetween('-30 days', 'now'),
				'created_at'=> $faker->dateTimeBetween('-60 days', 'now')
			]);

			$user->profile()->update([
				'user_id'	=> $user->id,
				'height'	=> $faker->numberBetween(150,200),
				'build'		=> $faker->randomElement(array_keys(ProfileOptions::$build)),
				'smoker'	=> $faker->randomElement(array_keys(ProfileOptions::$smoker)),
				'children'	=> $faker->randomElement(array_keys(ProfileOptions::$children)),
				'vegetarian'=> $faker->randomElement(array_keys(ProfileOptions::$vegetarian)),
				'education' => $faker->randomElement(array_keys(ProfileOptions::$education)),
				'about'		=> $faker->paragraph(3),
			]);

			$location = Location::firstOrCreate(['city' => $faker->randomElement($locations)]);
			$user->location()->associate($location);
			$user->save();
		}
	}

}
