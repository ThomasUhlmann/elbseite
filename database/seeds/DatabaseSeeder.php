<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('RolesTableSeeder');
		$this->call('UsersTableSeeder');
		$this->call('MessagesTableSeeder');
		$this->call('TagsTableSeeder');
		$this->call('UserInterestsTableSeeder');
		$this->call('PostsTableSeeder');
		$this->call('ActivitiesTableSeeder');
		$this->call('ActivityUserTableSeeder');
	}

}
