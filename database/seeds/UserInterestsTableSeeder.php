<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Elbsingles\Posts\Tags\Tag;
use Elbsingles\Users\User;

class UserInterestsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		$tags = Tag::where('parent_id', '<>', '')->get(['id'])->fetch('id')->toArray();

		$users = User::all();
		
		foreach($users as $user)
		{
			$user->interests()->sync( $faker->randomElements($tags, $faker->numberBetween(4, 10)) );
		}
	}

}
