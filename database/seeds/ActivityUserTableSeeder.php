<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Elbsingles\Activities\Activity;
use Elbsingles\Users\User;

class ActivityUserTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		$users = User::lists('id');

		$activities = Activity::all();

		foreach ($activities as $activity)
		{
			$activity->participants()->sync($faker->randomElements($users, $faker->numberBetween(0,10)));
		}
	}

}
