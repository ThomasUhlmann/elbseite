<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Elbsingles\Messages\Conversation;
use Elbsingles\Messages\Message;
use Elbsingles\Users\User;

class MessagesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
		$thomas = User::find(1);

		foreach (range(1, 30) as $con_index)
		{
			$user = User::find($con_index + 1);
			$messages = [];

			$conversation = Conversation::create([
				'isRead'	  => false,
				'last_writer' => 1
			]);
			$thomas->conversations()->save($conversation);
			$user->conversations()->save($conversation);

			foreach (range(1, $faker->numberBetween(2, 30)) as $mes_index) {
				$messages[] = [
					'conversation_id' => $conversation->id,
					'from_id'		  => $faker->randomElement([$thomas->id, $user->id]),
					'body'			  => $faker->paragraph(4),
					'created_at'	  => $faker->dateTimeBetween('-30 days', '-2 days')
				];
			}

			Message::insert($messages);

			// To ensure that the last message corresponds to the last writer
			Message::create([
				'conversation_id' => $conversation->id,
				'from_id'		  => $faker->randomElement([$thomas->id, $user->id]),
				'body'			  => $faker->paragraph(4),
				'created_at'	  => $faker->dateTimeBetween('-2 days', 'now')
			]);
		}
	}
}