<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Elbsingles\Users\Roles\Role;

class RolesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		$roles = [
			'admin',
			'member'
		];

		foreach($roles as $role)
		{
			Role::create(['name' => $role]);
		}
	}

}
