<?php namespace Elbsingles\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterUserRequest extends FormRequest {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'username'	=> ['required', 'unique:users', 'regex:/(^[a-zA-Zäöüß0-9_.]+)$/i'],
			'email'		=> ['required' , 'unique:users', 'email'],
			'password'	=> ['required', 'confirmed']
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Custom error messages
	 * @return array
	 */
	public function messages()
	{
		return [
			'username.required'	=> 'Bitte gib einen Nutzernamen an.',
			'username.unique'	=> 'Dieser Benutzername wird bereits verwendet',
			'username.regex'	=> 'Der Benutzername darf nur Buchstaben, Ziffern, Punkte und Unterstriche enthalten.',
			'email.required'	=> 'Bitte gib eine gültige E-Mailadresse ein',
			'email.unique'		=> 'Diese E-Mailadresse ist bereits registriert.',
			'email.email'		=> 'Bitte gib eine gültige E-Mailadresse ein',
			'password.required'	=> 'Bitte gib ein Passwort ein.',
			'password.confirmed'=> 'Die beiden Passwörter sind nicht identisch.'
		];
	}

}
