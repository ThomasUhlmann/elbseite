<?php

// Incident::listen('illuminate.query', function($query)
// {
// 	var_dump($query);
// });

/*===================================================
=            PUBLIC AREA / NOT LOGGED IN            =
===================================================*/

# Home
Route::get('/', ['as' => 'home', 'uses' => 'PagesController@getHome']);

# Impressum
Route::get('impressum', ['as' => 'impressum', 'uses' => 'PagesController@getImpressum']);

# Feedback
Route::post('feedback', ['as' => 'feedback', 'uses' => 'PagesController@postFeedback']);

# For Guests
Route::group(['middleware' => 'guest'], function()
{
	# Registration
	Route::get('mitmachen', ['as' => 'register', 'uses' => 'Users\RegistrationController@create']);
	Route::post('mitmachen', ['as' => 'register.store', 'uses' => 'Users\RegistrationController@store']);

	# Password reminder (redo!!!)
	Route::get('password/remind', ['as' => 'password.remind.show', 'uses' => 'Auth\RemindersController@getRemind']);
	Route::post('password/remind', ['as' => 'password.remind.mail', 'uses' => 'Auth\RemindersController@postRemind']);
	Route::get('password/reset/{token}', ['as' => 'password.reset.show', 'uses' => 'Auth\RemindersController@getReset']);
	Route::post('password/reset', ['as' => 'password.reset.store', 'uses' => 'Auth\RemindersController@postReset']);

	# Sessions - AJAX call
	Route::group(['middleware' => 'ajax'], function()
	{
		Route::post('login', 'Users\SessionController@store');
	});
});

/*=====================================
=            ADMIN SECTION            =
=====================================*/

Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function()
{
	# Dashboard
	Route::get('', ['as' => 'admin', 'uses' => 'PagesController@getAdmin']);

	# Manage Tags
	Route::resource('tags', 'TagsController', ['except' => 'show']);

	# Find users
	Route::get('users', ['as' => 'users', 'uses' => 'Users\ProfileController@index']);

	# Feedback
	Route::delete('feedback/{id}/delete', ['as' => 'feedback.delete', 'uses' => 'FeedbackController@destroy']);
});

/*=============================================
=            REGISTERED USERS ONLY            =
=============================================*/

Route::group(['middleware' => 'auth'], function()
{
	# Log Out
	Route::get('logout', ['as' => 'logout', 'uses' => 'Users\SessionController@logout']);

	# Show User Profile
	Route::get('@{username}', ['as' => 'profile', 'uses' => 'Users\ProfileController@show']);

	# Only accessable by profile owner
	Route::group(['middleware' => 'owner.only'], function()
	{
		# Edit profile
		Route::get('@{username}/edit', ['as' => 'profile.edit', 'uses' => 'Users\ProfileController@edit']);
		Route::put('@{username}', ['as' => 'profile.update', 'uses' => 'Users\ProfileController@update']);
		Route::put('@{username}/avatar', ['as' => 'profile.update.avatar', 'uses' => 'Users\AvatarController@update']);
		Route::put('@{username}/interests', ['as' => 'profile.update.interests', 'uses' => 'Users\ProfileController@updateInterests']);
		
		# Delete Profile
		Route::get('@{username}/delete', ['as' => 'profile.confirm.delete', 'uses' => 'Users\ProfileController@confirmDestroy']);
		Route::delete('@{username}', ['as' => 'profile.delete', 'uses' => 'Users\ProfileController@destroy']);
		
		# Manage Settings
		Route::put('@{username}/einstellungen', ['as' => 'user.settings.update', 'uses' => 'Users\SettingsController@update']);
	});

	# Conversations
	Route::get('nachrichten', ['as' => 'conversation.inbox', 'uses' => 'ConversationsController@getInbox']);
	Route::get('nachrichten/postausgang', ['as' => 'conversation.outbox', 'uses' => 'ConversationsController@getOutbox']);
	Route::get('nachrichten/archiv', ['as' => 'conversation.archive', 'uses' => 'ConversationsController@getArchive']);
	Route::get('nachrichten/{username}', ['as' => 'conversation.show', 'uses' => 'ConversationsController@show']);
	Route::post('nachrichten/{username}', ['as' => 'conversation.store', 'uses' => 'ConversationsController@store']);
	Route::post('nachrichten/{username}/archive', ['as' => 'conversation.doArchive', 'uses' => 'ConversationsController@archive']);

	# Notifications
	Route::delete('neuigkeiten/{id}', ['as' => 'notification.delete', 'uses' => 'NotificationsController@destroy']);

    # Find a Single
    Route::get('singles', ['as' => 'singles.index', 'uses' => 'SinglesController@index']);

	# Help Section
	Route::get('hilfe', ['as' => 'help', 'uses' => 'PagesController@getHelp']);

	# Events Calendar
	Route::get('kalender', ['as' => 'events.index', 'uses' => 'ActivitiesController@index']);

	# Favorites
	Route::get('merkliste', ['as' => 'favorite.index', 'uses' => 'Users\FavoritesController@index']);
	
	/*==================================================
	=            API to fetch and send data            =
	===================================================*/

	Route::group(['middleware' => 'ajax'], function() {

		# Stream
		Route::get('/api/stream', 'PostsController@getStream');

		# Posts
		Route::post('/api/post/store', 'PostsController@store');
		Route::delete('/api/post/{post}/delete', 'PostsController@destroy');

		# Activities
		Route::post('/api/activity/{activity}/join', 'ActivitiesController@join');

		# Tags
		Route::get('/api/interests', 'TagsController@getInterests');

		# Comments
		Route::post('/api/post/{post}/comments/store', 'CommentsController@store');
		Route::delete('/api/post/{post}/comments/{comment}/delete', 'CommentsController@destroy');
		Route::post('/api/activity/{activity}/comments/store', 'CommentsController@store');
		Route::delete('/api/activity/{activity}/comments/{comment}/delete', 'CommentsController@destroy');

		# Messages
		Route::get('/api/messages/{username}', 'ConversationsController@getConversation');
		Route::post('/api/messages/{username}/store', 'ConversationsController@postNewMessage');
		Route::post('/api/messages/{username}/delete', 'ConversationsController@delete');

		# Feedback
		Route::post('/api/feedback/store', 'FeedbackController@store');

		# Remember / Favorite
		Route::post('/api/favorites/{id}/remember', 'Users\FavoritesController@remember');

		# Block users
		Route::any('/api/user/{username}/block', 'Users\BlockUsersController@block');

		# Singles
		Route::get('/api/singles', 'SinglesController@getSingles');
	});
});