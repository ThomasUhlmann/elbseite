<?php namespace Elbsingles\Http\Controllers;

use Input, Redirect;
use Illuminate\Routing\Controller;
use Elbsingles\Posts\Tags\Tag;

class TagsController extends Controller {

	/**
	 * Display a listing of the resource.
	 * GET /tags
	 *
	 * @return Response
	 */
	public function index()
	{
		$tags = Tag::main()->with('children')->orderBy('name')->get();

		return view('admin.tags.index', compact('tags'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /tags/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$tags = Tag::main()->lists('name', 'id');
		$tags = [null => 'Als Oberkategorie festlegen'] + $tags;
		return view('admin.tags.create', compact('tags'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /tags
	 *
	 * @return Response
	 */
	public function store()
	{
		Tag::create([
			'name' 		=> Input::get('name'),
			'slug'		=> Input::get('slug'),
			'parent_id' => Input::get('parent_id')
			]);

		return Redirect::route('admin.tags.index');
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /tags/{tags}/edit
	 *
	 * @param  int  $tags
	 * @return Response
	 */
	public function edit($tags)
	{
		$tags = Tag::main()->lists('name', 'id');
		$tags = [null => 'Als Oberkategorie festlegen'] + $tags;
		return view('admin.tags.edit', compact('tags', 'tags'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /tags/{tags}
	 *
	 * @param  int  $tags
	 * @return Response
	 */
	public function update($tags)
	{
		$tags->fill(Input::only('name', 'slug', 'parent_id'))->save();

		return Redirect::route('admin.tags.index');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /tags/{tags}
	 *
	 * @param  int  $tags
	 * @return Response
	 */
	public function destroy($tags)
	{
		$tags->delete();

		return Redirect::route('admin.tags.index');
	}

	/*============================
	=            AJAX            =
	============================*/
	
	public function getInterests()
	{
		return Tag::all();
	}
}