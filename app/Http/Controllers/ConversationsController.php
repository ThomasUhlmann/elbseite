<?php namespace Elbsingles\Http\Controllers;

use Auth, Carbon, Config, Event, Input;
use Illuminate\Routing\Controller;
use Elbsingles\Messages\Conversation;
use Elbsingles\Messages\Message;

class ConversationsController extends Controller {

	public function getInbox()
	{
		$conversations = Auth::user()->conversations()
									 ->with('messages', 'users')
									 ->notDeleted()
									 ->inbox()
									 ->paginate(Config::get('elbseite.mailbox.conversations.paginate'));

		return view('pages.conversations.index', compact('conversations'));
	}

	public function getOutbox()
	{
		$conversations = Auth::user()->conversations()
									 ->with('messages', 'users')
									 ->notDeleted()
									 ->outbox()
									 ->paginate(Config::get('elbseite.mailbox.conversations.paginate'));

		return view('pages.conversations.index', compact('conversations'));
	}

	public function getArchive()
	{
		$conversations = Auth::user()->conversations()
									 ->with('messages', 'users')
									 ->notDeleted()
									 ->archive()
									 ->paginate(Config::get('elbseite.mailbox.conversations.paginate'));

		return view('pages.conversations.index', compact('conversations'));
	}

	public function show($user)
	{	
		if($user == Auth::user()) { return redirect()->route('conversation.inbox'); }

		$conversation = Auth::user()->conversations()
									->withUser($user)
									->first();

		if ($conversation) { $conversation->markAsRead(); }

		return view('pages.conversations.show', compact('user'));
	}

	public function archive($user)
	{
		$conversation = Auth::user()->conversations()
									->withUser($user)
									->first();


		$conversation->pivot->archived = true;
		$conversation->pivot->save();

		return redirect()->route('conversation.inbox');
	}

	public function delete($user)
	{
		$conversation = Auth::user()->conversations()
									->withUser($user)
									->first();

		$conversation->pivot->update([
			'deleted_at' => Carbon::now(),
			'trashed'	 => true
		]);

		return redirect()->route('conversation.inbox');
	}

	/*==========  AJAX  ==========*/
	
	public function getConversation($user)
	{
		$conversation = Auth::user()->conversations()
									->notDeleted()
									->withUser($user)
									->first();

		if ( ! $conversation)
		{
			$conversation = new Conversation;
		}

		$deleteDate = $conversation->pivot ? $conversation->pivot->deleted_at : Carbon::createFromTimeStamp(0);

		$messages = $conversation->messages()
								 ->after($deleteDate)
								 ->paginate(Config::get('elbseite.mailbox.messages.paginate'));

		return response([
				'messages' => $this->transform($messages->items()),
				'nextPageUrl' => $messages->nextPageUrl()
			], 200);
	}

	public function postNewMessage($user)
	{
		$conversation = Auth::user()->conversations()->withUser($user)->first();

		if ( ! $conversation )
		{
			$conversation = Conversation::startConversationWith($user);
		}

		$message = Message::addMessageTo($conversation, Input::get('body'))
						  ->load('sendFrom');

		$conversation->takeOutOfTrash()
					 ->takeOutOfArchive()
					 ->markAsUnread();

		// Event::fire('MessageReceived', $message);

		return response(['newMessage' => $this->transform([$message])], 200);
	}

	/*=================================
	=            Functions            =
	=================================*/

	protected function transform($messages)
	{
		return array_map(function($message) {
			return [
				'body' => $message['body'],
				'sendAt' => $message['created_at']->timestamp * 1000,
				'user' => $message['sendFrom']
			];
		}, $messages);
	}

}
