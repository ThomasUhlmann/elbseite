<?php namespace Elbsingles\Http\Controllers;

use Auth;
use Elbsingles\Comments\Comment;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class CommentsController extends Controller {

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request, $item)
	{
		$comment = Comment::create([
			'body'				=> $request->input('body'),
			'user_id'			=> Auth::user()->id,
			'commentable_id'	=> $item->id,
			'commentable_type'	=> get_class($item)
		]);

		$comment->load('user');

		return response($comment->toArray(), 200);
	}

	public function destroy($post, $comment)
	{
		if(Auth::user()->id == $comment->user_id || Auth::user()->hasRole('admin'))
		{
			$comment->delete();
		}

		return response(['success' => true], 200);
	}

}