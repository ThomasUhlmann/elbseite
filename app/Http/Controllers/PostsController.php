<?php namespace Elbsingles\Http\Controllers;

use Auth, Carbon, Config;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Elbsingles\Http\Requests\CreatePostRequest;
use Elbsingles\Posts\Post;
use Elbsingles\Activities\Activity;
use Elbsingles\Services\Stream\StreamBuilder;

class PostsController extends Controller {

	/*============================
	=            AJAX            =
	============================*/
	
	/**
	 * Get the Stream
	 */
	public function getStream(Request $request)
	{
		$stream = new StreamBuilder($request);
		$stream->execute();

		return response([
			'posts'		  => $this->transform($stream->getItems()),
			'nextPageUrl' => $stream->getNextPageUrl()
		], 200);
	}

	/**
	 * Store a new post
	 */
	public function store(CreatePostRequest $request)
	{
		$post = Post::create([
				'body'	  	  => $request->input('body'),
				'user_id'	  => Auth::user()->id,
				'category_id' => $request->input('category')['id'],
			]);

		$post->load('user', 'comments.user', 'category', 'linkable');

		return response(['newPost' => $this->transform([$post])], 200);
	}

	public function destroy($post)
	{
		if(Auth::user()->id == $post->user_id || Auth::user()->hasRole('admin'))
		{
			$post->delete();
		}

		return response(['success' => true], 200);
	}

	/*=================================
	=            Functions            =
	=================================*/

	protected function transform($collection)
	{
		$result = [];
		foreach($collection as $item) {

			$result[] = [
				'id'			=> $item->getId(),
				'type'			=> $item->getType(),
				'tags'			=> $item->getTags(),
				'body'			=> $item->getBody(),
				'user'			=> $item->getUser(),
				'favorited' 	=> $item->getFavorited(),
				'canDelete'		=> $item->getCanDelete(),
				'privateMessaging' => $item->getPrivateMessaging(),
				'comments'		=> $item->getComments(),
				'time'			=> $item->getTimestamp(),
				$item->getType()=> $item->getTypeSpecific(),
			];
		}
		return $result;
	}
}