<?php namespace Elbsingles\Http\Controllers;

use Auth;
use Elbsingles\Activities\Activity;
use Elbsingles\Http\Controllers\Controller;

class ActivitiesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$query = "{type: 'activity', sortBy: 'date', order: 'asc'}";

		return view('pages.activities.index', compact('events', 'query'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	/**
	 * Join an activity
	 */
	public function join($activity)
	{
		if ( $activity->participants->contains(Auth::user()) )
		{
			$activity->participants()->detach(Auth::user());
			return response([
				'joined' => false,
				'user'	=> Auth::user()->toJson(),
				], 200);
		}
		else
		{
			$activity->participants()->attach(Auth::user());
			return response([
				'joined' => true,
				'user'	=> Auth::user()->toJson(),
				], 200);
		}

	}

}
