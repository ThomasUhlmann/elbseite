<?php namespace Elbsingles\Http\Controllers\Users;

use Auth;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class SessionController extends Controller {

	/**
	 * Login
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$login = filter_var($request->input('login'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

		$attempt = Auth::attempt([
			$login			=> $request->input('login'),
			'password'		=> $request->input('password'),
		], $request->has('remember'));

		if($attempt)
		{
			return response(['login' => true], 200);
		}

		return response(['login' => false], 200);
	}

	/**
	 * Logout
	 *
	 * @return Response
	 */
	public function logout()
	{
		Auth::logout();

		return redirect()->home();
	}

}
