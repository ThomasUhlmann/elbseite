<?php namespace Elbsingles\Http\Controllers\Users;

use Event;
use Illuminate\Routing\Controller;
use Elbsingles\Http\Requests\RegisterUserRequest;
use Elbsingles\Users\User;

class RegistrationController extends Controller {

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('pages.registration.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RegisterUserRequest $request)
	{
		$user = User::create($request->input());

		Event::fire('UserHasRegistered', [$user]);
		return redirect()->route('home');
	}
}
