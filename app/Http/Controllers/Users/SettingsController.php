<?php namespace Elbsingles\Http\Controllers\Users;

use Input;
use Illuminate\Routing\Controller;

class SettingsController extends Controller {

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($user)
	{
		$user->load('settings', 'profile');
		return view('pages.users.settings.edit', compact('user'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($user)
	{
		$user->settings
			 ->fill(Input::only(
				'showPostsInProfile',
				'sendDailyMail',
				'wantsToFlirt',
				'genderInterest'))
			 ->save();

		return redirect()->route('profile', [$user->username]);
	}

}
