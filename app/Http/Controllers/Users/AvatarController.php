<?php namespace Elbsingles\Http\Controllers\Users;

use Auth, File, Image, Input, Redirect;
use Elbsingles\Http\Controllers\Controller;

class AvatarController extends Controller {

	/**
	 * Update the avatar in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($user)
	{
		$this->deleteOldAvatar($user);
		$this->saveNewAvatar($user);

		return Redirect::route('profile', [$user->username]);
	}

	protected function saveNewAvatar($user)
	{
		$image = Input::file('avatar');
		$storagePath = public_path() . '/img/avatars/' . strtolower($user->username) . '/';
		$filename = str_random(12) . '.' . $image->guessExtension();

		File::exists($storagePath) or File::makeDirectory($storagePath);

		$avatar = Image::make($image->getRealPath());

		$avatar->fit(200,200)->save($storagePath . $filename)
			   ->fit(50,50)->save($storagePath . 'small-' . $filename);

		$user->avatar = $filename;
		$user->save();
	}

	protected function deleteOldAvatar($user)
	{
		if($user->avatar)
		{
			File::delete(avatar_path($user) . $user->avatar);
			File::delete(avatar_path($user) . 'small-' . $user->avatar);
		}
	}



}
