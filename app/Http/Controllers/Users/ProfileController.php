<?php namespace Elbsingles\Http\Controllers\Users;

use Auth, Datatables;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Elbsingles\Users\User;
use Elbsingles\Users\Locations\Location;
use Elbsingles\Posts\Tags\Tag;

class ProfileController extends Controller {

	/**
	 * Display all the users.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function index()
	{
		if(Request::wantsJson())
		{
			$users = User::select(['users.username', 'users.dob', 'users.gender', 'users.email', 'roles.name'])
						 ->leftJoin('role_user', 'users.id', '=', 'role_user.user_id')
						 ->leftJoin('roles', 'role_user.role_id', '=', 'roles.id');

			return Datatables::of($users)
							 ->edit_column('username', function($user) {
									return $user->present()->profileLink;
							   })
						 	 ->edit_column('dob', function($user) {
									return $user->age;
							   })
						 	 ->edit_column('gender', function($user) {
									return $user->gender == 'female' ? 'Frau' : 'Mann';
							   })
							 ->make();
		}

		return view('admin.users.index', compact('user'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(User $user)
	{
		$user->load('profile', 'interests', 'location', 'hasBlocked', 'blockedBy');

		$query = "{username: '$user->username'}";

		return view('pages.users.show', compact('user', 'query'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($user)
	{
		$user->load('profile', 'interests');
		$tags = Tag::main()->with('children')->get();

		return view('pages.users.edit', compact('user', 'tags'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($user, Request $request)
	{
		// Update Profile
		$user->profile
			 ->fill($request->only('height', 'build', 'smoker', 'vegetarian', 'children', 'education', 'about'))
			 ->save();

		// Update Location
		if($request->has('location'))
		{
			$location = Location::firstOrCreate(['city' => $request->get('location')]);
			$user->location()->associate($location);
		}
		else
		{
			$user->location_id = null;
		}

		// Update Date of Birth
		$user->dob = $request->get('dob');
		$user->save();

		return redirect()->route('profile', [$user->username]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateInterests($user, Request $request)
	{
		$interests = $request->has('interests') ? $request->get('interests') : [];

		$user->interests()->sync($interests);

		return redirect()->back();
	}

	public function confirmDestroy($user)
	{
		return view('pages.users.destroy', compact('user'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($user, Request $request)
	{
		$loginSuccess = Auth::attempt(['username' => $user->username, 'password' => $request->get('password')]);
		if( $user->isAuthUser() && $loginSuccess )
		{
			$user->delete();
			return redirect()->route('home');
		}

		return redirect()->back();
	}

}
