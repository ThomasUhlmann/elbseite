<?php namespace Elbsingles\Http\Controllers\Users;

use Auth, Input;
use Illuminate\Routing\Controller;

class BlockUsersController extends Controller {

	/*============================
	=            AJAX            =
	============================*/
	
	public function block($user)
	{
		if(Auth::user()->hasBlocked->contains($user))
		{
			Auth::user()->hasBlocked()->detach($user);
			return response(['blocked' => false], 200);
		}
		else
		{
			Auth::user()->hasBlocked()->attach($user);
			return response(['blocked' => true], 200);
		}
	}
}
