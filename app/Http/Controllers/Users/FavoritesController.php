<?php namespace Elbsingles\Http\Controllers\Users;

use Auth;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class FavoritesController extends Controller {

	/**
	*
	* Show page with all favorited posts and users
	*
	**/
	public function index()
	{
		$favoritedUsers = Auth::user()->favoritedUsers;
		$favoritedPosts = Auth::user()->favoritedPosts;

		return view('pages.favorites.index', compact('favoritedPosts', 'favoritedUsers'));
	}

	/*============================
	=            AJAX            =
	============================*/

	/**
	*
	* Toggle favorites
	*
	**/
	public function remember(Request $request, $id)
	{
		$favoritedType = $this->getRelationship($request->get('type'));

		if (Auth::user()->{$favoritedType}->contains($id))
		{
			Auth::user()->{$favoritedType}()->detach($id);
			return response(['favorited' => false], 200);
		}
		else
		{
			Auth::user()->{$favoritedType}()->attach($id);
			return response(['favorited' => true], 200);
		}
	}

	protected function getRelationship($type)
	{
		if(substr($type, -1) == 'y')
		{
			$type = substr($type, 0, strlen($type) - 1) . 'ie';
		}

		return 'favorited' . ucfirst($type) . 's';
	}
}