<?php namespace Elbsingles\Http\Controllers;

use Auth, Event;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Elbsingles\Users\User;
use Elbsingles\Feedback\Feedback;

class PagesController extends Controller {

	public function getHome()
	{
		if(Auth::guest())
		{
			return view('pages.home.guest');
		}

		$query = '{tags: ' . json_encode(Auth::user()->interests->unique()->lists('id'), JSON_NUMERIC_CHECK ) . '}';

		return view('pages.home.auth', compact('query'));
	}

	public function getImpressum()
	{
		return view('pages.impressum');
	}


	public function getHelp()
	{
		return view('pages.help.index');
	}


	public function getAdmin()
	{
		$users = User::get(['gender', 'last_active', 'created_at']);
		$feedbackCollection = Feedback::with('user')->unprocessed()->latest()->get();
		return view('admin.home', compact('users', 'feedbackCollection'));
	}
}
