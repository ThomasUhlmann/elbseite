<?php namespace Elbsingles\Http\Controllers;

use Auth, Event;
use Illuminate\Http\Request;
use Elbsingles\Http\Controllers\Controller;
use Elbsingles\Feedback\Feedback;

class FeedbackController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$feedback = Feedback::create([
			'user_id' => Auth::user()->id,
			'message' => $request->input('message')
		]);

		Event::fire('FeedbackFormSubmitted', $feedback);

		return response(['received' => isset($feedback)], 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$feedback = Feedback::find($id);
		$feedback->delete();
		return redirect()->back();
	}

}
