<?php namespace Elbsingles\Http\Controllers;

use Auth, Carbon;
use Elbsingles\Services\Timer\Timer;
use Elbsingles\Users\User;
use Elbsingles\Users\Profiles\ProfileOptions;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class SinglesController extends Controller {

	public function index()
	{
        return view('pages.singles.index');
	}


	/*============================
	=            AJAX            =
	=============================*/
	
	public function getSingles(Request $request)
	{
		$singles = User::with('location', 'interests', 'profile')
					   ->thatWantToFlirt()
					   ->matchSexuality()
					   ->without(Auth::user())
					   ->lastActive(Carbon::now()->subDays(90))
					   ->latest('last_active')
					   ->get();

		return response([
			'user' => Auth::user(),
			'singles' => $this->transform($singles)
			], 200);
	}

	/*=================================
	=            Functions            =
	=================================*/
	
	protected function transform($singles)
	{
		$results = [];

		foreach ($singles as $single)
		{
			$last_active = with(new Timer($single->last_active))->getLastActive();

			if($single->location && Auth::user()->location)
			{
				$distance = $single->location->distance(Auth::user()->location);
				$location = ['city' => $single->location->city, 'distance' => $distance];
			}
			else
			{
				$location = [];
			}

			$profile = [];
			$profile['height'] = $single->present()->height;
			$profile['build'] = $single->present()->attribute('build');
			$profile['children'] = $single->present()->attribute('children');
			$profile['smoker'] = $single->present()->attribute('smoker');
			$profile['education'] = $single->present()->attribute('education');
			$profile['vegetarian'] = $single->present()->attribute('vegetarian');
			
			$results[] = [
				'username'	=> $single->username,
				'avatar'	=> $single->avatar,
				'gender'	=> $single->gender,
				'age'		=> $single->age,
				'last_active'=>$last_active,
				'location'	=> $location,
				'interests'	=> $single->interests,
				'profile'	=> $profile,
			];
		}

		return $results;
	}
	
	
	

}
