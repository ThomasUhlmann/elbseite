<?php namespace Elbsingles\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {

	/**
	 * The application's global HTTP middleware stack.
	 *
	 * @var array
	 */
	protected $middleware = [
		'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
		'Illuminate\Cookie\Middleware\EncryptCookies',
		'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
		'Illuminate\Session\Middleware\StartSession',
		'Illuminate\View\Middleware\ShareErrorsFromSession',
		'Illuminate\Foundation\Http\Middleware\VerifyCsrfToken',
	];

	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'admin'		=> 'Elbsingles\Http\Middleware\IsAdmin',
		'ajax'		=> 'Elbsingles\Http\Middleware\IsAjax',
		'auth'		=> 'Elbsingles\Http\Middleware\Authenticate',
		'auth.basic'=> 'Illuminate\Auth\Middleware\AuthenticateWithBasicAuth',
		'guest'		=> 'Elbsingles\Http\Middleware\RedirectIfAuthenticated',
		'owner.only'=> 'Elbsingles\Http\Middleware\IsOwner',
	];
}
