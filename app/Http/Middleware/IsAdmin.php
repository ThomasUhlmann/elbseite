<?php namespace Elbsingles\Http\Middleware;

use Auth, Closure;

class IsAdmin {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (Auth::guest() or ! Auth::user()->hasRole('admin'))
		{
			return redirect()->route('home');
		}

		return $next($request);
	}

}
