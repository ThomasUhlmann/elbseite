<?php namespace Elbsingles\Http\Middleware;

use Auth, Closure;

class IsOwner {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$user = $request->route()->getParameter('username');

		if(Auth::user() != $user)
		{
			return redirect()->route('home');
		}

		return $next($request);
	}

}
