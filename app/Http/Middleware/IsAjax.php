<?php namespace Elbsingles\Http\Middleware;

use App, Closure;

class IsAjax {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ( ! $request->ajax() && App::environment() != 'local')
		{
			return redirect()->route('home');
		}

		return $next($request);
	}

}
