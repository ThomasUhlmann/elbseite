<?php namespace Elbsingles\Activities;

use Auth, Carbon;
use Illuminate\Database\Eloquent\Model;
use Elbsingles\Services\Stream\StreamFilters;
use Elbsingles\Services\Stream\StreamInterface;

class Activity extends Model implements StreamInterface {

	use StreamFilters, StreamActivityTrait;

	/*=============================
	=            Setup            =
	=============================*/

	protected $fillable = ['title', 'user_id', 'body', 'privateMessaging','date', 'location'];

	protected $visible = ['title', 'body', 'privateMessaging', 'date', 'location'];

	/* Dates transformed to Carbon object */
	public function getDates()
	{
	    return ['date', 'updated_at', 'created_at'];
	}

	/*==========  Setters & Getters  ==========*/

    public function getFavoritedAttribute()
	{
		if(Auth::guest())
		{
			return false;
		}

		return Auth::user()->favoritedActivities->contains($this->id);
	}

	public function getCanDeleteAttribute()
	{
		if(Auth::user()->id == $this->user_id || Auth::user()->hasRole('admin'))
		{
			return true;
		}

		return false;
	}

	/*==============================================
	=            Database relationships            =
	==============================================*/

	public function user()
	{
		return $this->belongsTo('Elbsingles\Users\User');
	}
	
	public function tags()
	{
		return $this->morphToMany('Elbsingles\Posts\Tags\Tag', 'taggable');
	}

	public function comments()
	{
		return $this->morphMany('Elbsingles\Comments\Comment', 'commentable')->oldest();
	}

	public function participants()
	{
		return $this->belongsToMany('Elbsingles\Users\User');
	}

	public function favoritedBy()
	{
		return $this->morphToMany('Elbsingles\Users\User', 'favoritable', 'favorites');
	}

	/*====================================
	=            Query Scopes            =
	====================================*/
	
	public function scopeUpcoming($query)
	{
		return $query->where('date', '>=', Carbon::now());
	}

}