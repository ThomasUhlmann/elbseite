<?php namespace Elbsingles\Activities;

use Carbon;

trait StreamActivityTrait {

	public function getId()
	{
		return $this->id;
	}

	public function getType()
	{
		return 'activity';
	}

	public function getTypeSpecific()
	{
		return [
			'title' 	=> $this->title,
			'date'  	=> $this->date->timestamp * 1000,
			'location'  => $this->location,
			'participants'=> $this->participants,
		];
	}

	public function getTags()
	{
		return $this->tags;
	}

	public function getBody()
	{
		return $this->body;
	}

	public function getUser()
	{
		return $this->user;
	}

	public function getFavorited()
	{
		return $this->favorited;
	}

	public function getCanDelete()
	{
		return $this->canDelete;
	}

	public function getPrivateMessaging()
	{
		return (boolean) $this->privateMessaging;
	}

	public function getComments()
	{
		return $this->comments;
	}

	public function getTimestamp()
	{
		return $this->created_at->timestamp * 1000;
	}
}