<?php namespace Elbsingles\Services\Geo;

use GuzzleHttp\Exception\BadResponseException;

class Geo {

	public function fetchLocation($city, $output = 'json')
	{
		$provider = 'http://maps.google.com/maps/api/geocode/';

		$settings = [
			'address' => $city,
			'components' => 'country:DE',
			'sensor' => 'false'
		];

		$url = $provider . $output . '?' . http_build_query($settings);

		if( $response = with(new \GuzzleHttp\Client())->get($url)->json() )
		{
			$lat = array_get($response, 'results.0.geometry.location.lat');
			$lng = array_get($response, 'results.0.geometry.location.lng');
			return compact('city', 'lat', 'lng');
		}

		throw new BadResponseException;
	}

	public function distance($lat1, $lng1, $lat2, $lng2)
	{
		$pi80 = M_PI / 180;
		$lat1 = $lat1 * $pi80;
		$lng1 = $lng1 * $pi80;
		$lat2 = $lat2 * $pi80;
		$lng2 = $lng2 * $pi80;
	 
		$r = 6372.797; // mean radius of Earth in km
		$dlat = $lat2 - $lat1;
		$dlng = $lng2 - $lng1;
		$a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlng / 2) * sin($dlng / 2);
		$c = 2 * atan2(sqrt($a), sqrt(1 - $a));
		$km = $r * $c;
	 
		return $km;
	}
}