<?php namespace Elbsingles\Services\Timer;

use Carbon;

class Timer {

	protected $time;

	protected $lastActiveText = [
		'online'	=> ['text' => 'online', 'css' => 'label-success'],
		'today'		=> ['text' => 'Heute', 'css' => 'label-warning'],
		'yesterday'	=> ['text' => 'Gestern', 'css' => 'label-warning'],
		'2daysAgo'	=> ['text' => 'Vor zwei Tagen', 'css' => 'label-warning'],
		'3daysAgo'	=> ['text' => 'Vor drei Tagen', 'css' => 'label-warning'],
		'thisWeek'	=> ['text' => 'Diese Woche', 'css' => 'label-warning'],
		'lastWeek'	=> ['text' => 'Letzte Woche', 'css' => 'label-primary'],
		'2weeksAgo'	=> ['text' => 'Vor zwei Wochen', 'css' => 'label-primary'],
		'3weeksAgo'	=> ['text' => 'Vor drei Wochen', 'css' => 'label-primary'],
		'thisMonth'	=> ['text' => 'Diesen Monat', 'css' => 'label-primary'],
		'lastMonth'	=> ['text' => 'Letzten Monat', 'css' => 'label-primary'],
		'2monthsAgo'=> ['text' => 'Vor zwei Monaten', 'css' => 'label-primary'],
		'3monthsAgo'=> ['text' => 'Vor drei Monaten', 'css' => 'label-primary'],
		'inactive'	=> ['text' => 'Inaktiv', 'css' => 'label-default']
	];

	protected function defineMapping()
	{
		$this->mapping = [];
		$this->mapping['online']	= Carbon::now()->subMinutes(20);
		$this->mapping['today']		= Carbon::today();
		$this->mapping['yesterday'] = Carbon::yesterday();
		$this->mapping['2daysAgo'] 	= Carbon::yesterday()->subDays(1);
		$this->mapping['3daysAgo'] 	= Carbon::yesterday()->subDays(2);
		$this->mapping['thisWeek'] 	= Carbon::now()->startOfWeek();
		$this->mapping['lastWeek'] 	= Carbon::now()->startOfWeek()->subWeeks(1);
		$this->mapping['2weeksAgo'] = Carbon::now()->subWeeks(2);
		$this->mapping['3weeksAgo'] = Carbon::now()->subWeeks(3);
		$this->mapping['thisMonth'] = Carbon::now()->startOfMonth();
		$this->mapping['lastMonth'] = Carbon::now()->startOfMonth()->subMonths(1);
		$this->mapping['2monthsAgo']= Carbon::now()->subMonths(2);
		$this->mapping['3monthsAgo']= Carbon::now()->subMonths(3);
	}

	public function __construct(Carbon $time = null)
	{
		$this->time = $time ?: Carbon::now();

		$this->defineMapping();
	}

	public function lastActive()
	{
		foreach ($this->mapping as $class => $date)
		{
			if ($this->time->timestamp >= $date->timestamp )
			{
				return $class;
			}
		}

		return 'inactive';
	}

	public function getLastActive()
	{
		return $this->lastActiveText[$this->lastActive()];
	}

}