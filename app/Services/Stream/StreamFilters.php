<?php namespace Elbsingles\Services\Stream;

use Auth;

trait StreamFilters {

	public function scopeBlocked($query)
	{
		$hasBlocked = Auth::user()->hasBlocked;
		$blockedBy = Auth::user()->blockedBy;
		
		$blocked = $hasBlocked->toBase()->merge($blockedBy);

		if ( ! $blocked->isEmpty() )
		{
			return $query->whereNotIn('user_id', $blocked->lists('id'));
		}
	}

	public function scopeFilterByPeriod($query, $from, $to, $order, $column = 'created_at')
	{
		if($from) $query->where($column, $this->getOperator($order), $from);

		if($to) $query->where($column, $this->getOperator($order, false), $to);

		return $query;
	}

	public function getOperator($order, $from = true)
	{
		if ($from)
		{
			return $order == 'desc' ? '<' : '>';
		}

		return $order == 'desc' ? '>' : '<';
	}

	public function scopeFilterByUser($query, $username)
	{
		if ( ! empty($username))
		{
			return $query->whereHas('user', function($query) use ($username) {
				$query->where('username', $username);
			});
		}

		return $query;
	}

	public function scopeFilterByTags($query, $tags)
	{
		if( ! empty($tags))
		{
			return $query->whereHas('tags', function($query) use ($tags) {
				$query->whereIn('id', $tags);
			});
		}

		return $query;
	}

	public function scopeOrderItems($query, $column, $order)
	{
		$column = $column ?: 'created_at';
		$order = $order ?: 'desc';

		return $query->orderBy($column, $order);
	}
}
