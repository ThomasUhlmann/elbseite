<?php namespace Elbsingles\Services\Stream;

use Config, Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class StreamBuilder {

	/**
	 * Dictionary for the class names
	 */
	protected $dictionary = [
		'post'		=> 'Elbsingles\Posts\Post',
		'activity'	=> 'Elbsingles\Activities\Activity'
	];

	/**
	 * Holds the retrieved models
	 */
	protected $models;

	/**
	 * Holds the collection of items
	 * that will be shown in the stream
	 */
	protected $collection;

	/**
	 * Holds the query
	 */
	protected $query;

	/**
	 * Make sure that the pagination only runs once
	 */
	protected $paginationLock = false;

	/**
	 * We store the LengthAwarePaginator
	 * to have access to its functionality
	 */
	protected $lengthAwarePaginator;

	/**
	 * Which models should be retrieved
	 */
	protected $types;

	/**
	 * Defines a treshold from when to query the model
	 */
	protected $treshold;

	/**
	 * Holds the begin of the period
	 */
	protected $from;

	/**
	 * Holds the end of the period
	 */
	protected $to;

	/**
	 * The column that defines the order of
	 * the items in the stream.
	 */
	protected $sortColumn;

	/**
	 * Defines how to order the items
	 */
	protected $order;

	/**
	 * The tags for filtering the stream
	 */
	protected $tags;

	/**
	 * Whose posts should be shown
	 */
	protected $user;

	/**
	 * Constructor
	 */
	public function __construct(Request $request)
	{
		$this->query = $request->query();
		$this->types = $request->has('type') ? (array) $request->get('type') : array_keys($this->dictionary);
		$this->treshold = $request->get('treshold');
		$this->from = $this->getTreshold();
		$this->to = $request->get('to');
		$this->sortColumn = $request->get('sortBy') ? : 'created_at';
		$this->order = $request->get('order') ? : 'desc';
		$this->tags = $request->get('tags');
		$this->user = $request->get('username');
	}

	/**
	 * Build the stream
	 */
	public function execute()
	{
		$this->retrieveModels();
		$this->mergeModels();
		$this->sortCollection();
		$this->buildNextPageUrl();
	}

	/**
	 * Returns the next page url
	 */
	public function getNextPageUrl()
	{
		if ($this->lengthAwarePaginator)
		{
			if($this->lengthAwarePaginator->nextPageUrl() == null) return null;
			
			// Always return page one as we always define a new query
			return $this->lengthAwarePaginator->url(1);
		}

		return null;
	}

	/**
	 * Returns the items in the stream
	 */
	public function getItems()
	{
		return $this->collection;
	}

	/**
	 * Retrieves the models
	 */
	protected function retrieveModels()
	{
		foreach ($this->types as $type)
		{
			$this->models[] = $this->queryModel($type);
		}
	}

	/**
	 * Queries the models
	 */
	protected function queryModel($class)
	{
		$class = $this->translateClass($class);

		$query = $class::with('user', 'comments.user', 'tags')
					->filterByPeriod($this->from, $this->to, $this->order, $this->sortColumn)
					->filterByTags($this->tags)
					->filterByUser($this->user)
					->blocked()
					->orderItems($this->sortColumn, $this->order);

		return $this->performQuery($query);
	}

	/**
	 * Performs the Query
	 */
	protected function performQuery($query)
	{
		if( ! $this->paginationLock )
		{
			$result = $query->paginate(Config::get('elbseite.stream.paginate'));

			if ( ! empty($result->items()) )
			{
				$this->paginationLock = true;
				$this->lengthAwarePaginator = $result; // ??
				$this->setTreshold($result->items());
			}

			return $result;
		}

		return $query->get();
	}

	/**
	 * Translates the class to the
	 * namespaced class
	 */
	protected function translateClass($class)
	{
		if( ! isset($this->dictionary[$class]) )
		{
			throw new Exception; 
		}

		return $this->dictionary[$class];
	}

	/**
	 * Merges the models to one collection
	 */
	protected function mergeModels()
	{
		$this->collection = new Collection([]);

		foreach ($this->models as $model)
		{
			foreach ($model->all() as $item)
			{
				$this->collection->add($item);
			}
		}
	}

	/**
	 * Sorts the collection
	 */
	protected function sortCollection()
	{
		$column = $this->sortColumn ? : 'created_at';
		$descending = $this->order != 'asc';

		$this->collection->sortBy($column, SORT_REGULAR, $descending);
	}

	/**
	 * Sets the query string for the next page url
	 */
	protected function buildNextPageUrl()
	{
		$this->query['treshold'] = $this->to;

		if($this->lengthAwarePaginator)
		{
			$this->lengthAwarePaginator->appends($this->query);
		}
	}

	/**
	 * Sets a treshold for retrieving further models
	 * after querying the first model
	 */
	protected function setTreshold($collection)
	{	
		$lastItem = array_pop($collection);

		if ($lastItem)
		{
			$this->to = $lastItem->{$this->sortColumn}->format('Y-m-d H:i:s');
		}
	}

	/**
	 * Gets the treshold from the query string
	 */
	protected function getTreshold()
	{
		if($this->treshold) return $this->treshold;

		if($this->from) return $this->from;

		return null;
	}
}