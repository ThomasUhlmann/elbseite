<?php namespace Elbsingles\Services\Stream;

interface StreamInterface {

	public function getId();

	public function getType();

	public function getTypeSpecific();

	public function getTags();

	public function getBody();

	public function getUser();

	public function getFavorited();

	public function getCanDelete();

	public function getPrivateMessaging();

	public function getComments();

	public function getTimestamp();

}