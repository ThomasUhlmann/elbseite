<?php namespace Elbsingles\Services\Facebook;

use Illuminate\Database\Eloquent\Model;

class FacebookPage extends Model {

	protected $fillable = ['name', 'category_id'];

	/**
	 * Database relationships
	 */

	public function category()
	{
		return $this->belongsTo('Elbsingles\Posts\Category');
	}

}
