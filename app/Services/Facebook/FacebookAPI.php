<?php namespace Elbsingles\Services\Facebook;

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphPage;
use Elbsingles\Posts\PostCollection;
use Elbsingles\Posts\FacebookPost;
use Carbon;

class FacebookAPI {

	protected $session;
	protected $graph;
	protected $categoryId;

	public function __construct(FacebookSession $session)
	{
		$this->session = $session;
	}

	public function readGraph($page, $parameters = ['fields' => 'feed.limit(10){message,created_time},picture,name,link'])
	{
		$this->graph = (new FacebookRequest($this->session, 'GET', "/{$page}/?" . http_build_query($parameters)))
						->execute()
						->getGraphObject(GraphPage::className());

		return $this;
	}

	public function linkToCategory($categoryId)
	{
		$this->categoryId = $categoryId;

		return $this;
	}

	public function toPostCollection()
	{
		$collection = [];

		foreach ($this->getFeed() as $post)
		{
			if( isset($post->message) )
			{
				$collection[] = new FacebookPost([
						'name' => $this->graph->getProperty('name'),
						'body' => $post->message,
						'picture' => $this->graph->getProperty('picture')->getProperty('url'),
						'link' => $this->graph->getProperty('link'),
						'category_id' => $this->categoryId,
						'created_at' => Carbon::parse($post->created_time)
					]);
			}
		}

		return new PostCollection($collection);
	}

	private function getFeed()
	{
		return $this->graph->getProperty('feed')->getProperty('data')->asArray();
	}

}