<?php namespace Elbsingles\Services\Facebook;

use Illuminate\Support\Facades\Facade;

class Facebook extends Facade {

    /**
     * Name of IoC binding is...
     *
     * @return string
     */
    public static function getFacadeAccessor()
    {
        return 'facebook';
    }
}