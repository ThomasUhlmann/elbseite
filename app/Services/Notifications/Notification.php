<?php namespace Elbsingles\Services\Notifications;

use Laracasts\Presenter\PresentableTrait;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

	use PresentableTrait;

	protected $presenter = 'Elbsingles\Services\Notifications\NotificationPresenter';

	protected $fillable   = ['user_id', 'subject', 'body', 'notifiable_id', 'notifiable_type'];

	/**
	 * Database relationship
	 */

	public function user()
	{
		return $this->belongsTo('Elbsingles\Users\User');
	}

	public function notifiable()
	{
	    return $this->morphTo();
	}

	/**
	 * Scopes
	 */

	public function scopeUnread($query)
	{
		return $query->where('is_read', '=', 0);
	}

	/**
	 * Functions
	 */

	public function withSubject($subject)
	{
		$this->subject = $subject;

		return $this;
	}

	public function withBody($body)
	{
		$this->body = $body;

		return $this;
	}

	public function regarding($model)
	{
		if(is_object($model))
		{
			$this->notifiable_id   = $model->id;
			$this->notifiable_type = get_class($model);
		}

		return $this;
	}

	public function deliver()
	{
		$this->save();

		return $this;
	}

}
