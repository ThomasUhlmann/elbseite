<?php namespace Elbsingles\Services\Notifications;

use Laracasts\Presenter\Presenter;

class NotificationPresenter extends Presenter {

	private $partialsPath = 'pages.profile.partials.notifications.';

	public function include_path()
	{
		$class = $this->getClassName();

		$class = $this->toSnakeCase($class);

		return $this->partialsPath . $class;
	}

	public function getClassName()
	{
		return substr($this->notifiable_type, strrpos($this->notifiable_type, '\\') + 1, strlen($this->notifiable_type));
	}

	public function toSnakeCase($class)
	{
		for ($pos = 1; $pos < strlen($class); $pos++)
		{ 
			if(ctype_upper($class{$pos}))
			{
				$class = substr_replace($class, '_', $pos, 0);
				$pos++;
			}
		}

		return strtolower($class);
	}
}