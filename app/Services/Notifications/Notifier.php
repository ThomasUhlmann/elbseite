<?php namespace Elbsingles\Services\Notifications;

use Auth;
use Elbsingles\Users\User;
use Elbsingles\Messages\Message;

class Notifier {

	public function messageReceived(Message $message)
	{
		$sender = User::find($message->from_id);
		$receiver = User::find($message->to_id);

		$receiver->newNotification()
				 ->withSubject('Neue Nachricht von ' . $sender->username)
				 ->withBody( substr($message->body, 0, 32) )
				 ->regarding($message)
				 ->deliver();
	}
}