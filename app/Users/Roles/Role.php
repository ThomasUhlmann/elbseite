<?php namespace Elbsingles\Users\Roles;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {

    protected $fillable = ['name'];

}