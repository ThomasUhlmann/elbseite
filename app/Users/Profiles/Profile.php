<?php namespace Elbsingles\Users\Profiles;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model {

	protected $table = 'user_profiles';

	protected $fillable = [
		'user_id',
		'height',
		'build',
		'smoker',
		'children',
		'vegetarian',
		'education',
		'about'
	];

	/*==========  Getters & Setters  ==========*/

	public function setBuildAttribute($value)
	{
		if( $value !== null && ! in_array($value, array_keys(ProfileOptions::$build)) )
		{
			throw new \InvalidArgumentException;
		}

		$this->attributes['build'] = $value;
	}

	public function setSmokerAttribute($value)
	{
		if( $value !== null && ! in_array($value, array_keys(ProfileOptions::$smoker)) )
		{
			throw new \InvalidArgumentException;
		}

		$this->attributes['smoker'] = $value;
	}

	public function setChildrenAttribute($value)
	{
		if( $value !== null && ! in_array($value, array_keys(ProfileOptions::$children)) )
		{
			throw new \InvalidArgumentException;
		}

		$this->attributes['children'] = $value;
	}

	public function setVegetarianAttribute($value)
	{
		if( $value !== null && ! in_array($value, array_keys(ProfileOptions::$vegetarian)) )
		{
			throw new \InvalidArgumentException;
		}

		$this->attributes['vegetarian'] = $value;
	}

	public function setEducationAttribute($value)
	{
		if( $value !== null && ! in_array($value, array_keys(ProfileOptions::$education)) )
		{
			throw new \InvalidArgumentException;
		}

		$this->attributes['education'] = $value;
	}

	/*==============================================
	=            Database Relationships            =
	==============================================*/
	
	public function user()
	{
		return $this->belongsTo('Elbsingles\Users\User');
	}

	/*=================================
	=            Functions            =
	=================================*/
	
	public function hasInfos()
	{
		$dontShow = array_fill_keys(['id', 'user_id', 'about', 'created_at', 'updated_at'], null);
		$attributes = $this->getAttributes();

		foreach (array_diff_key($attributes, $dontShow) as $attr)
		{
			if($attr) return true;
		}
	}
}