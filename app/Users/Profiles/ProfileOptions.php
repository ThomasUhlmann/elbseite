<?php namespace Elbsingles\Users\Profiles;

class ProfileOptions {

	public static $build = [
		null => '-- Keine Angabe --',
		'1'	=> 'Schlank',
		'2' => 'Athletisch',
		'3' => 'Normal',
		'4'	=> 'Ein paar Kilo mehr',
		'5'	=> 'Durchaus beachtlich'
	];


	public static $smoker = [
		null => '-- Keine Angabe --',
		'1' => 'Ich bin Nichtraucher',
		'2'	=> 'Ich bin Raucher'
	];

	public static $children = [
		null => '-- Keine Angabe --',
		'1' => 'Ich habe mehrere Kinder',
		'2'	=> 'Ich habe ein Kind',
		'3'	=> 'Ich möchte irgendwann Kinder',
		'4' => 'Ich bin noch unentschlossen',
		'5'	=> 'Ich möchte keine Kinder'
	];

	public static $vegetarian = [
		null => '-- Keine Angabe --',
		'1'	=> 'vegan',
		'2' => 'vegetarisch'
	];

	public static $education = [
		null => '-- Keine Angabe --',
		'1'	=> 'ohne Abschluss',
		'2'	=> 'Realschule',
		'3'	=> 'Hochschulreife',
		'4'	=> 'abgeschlossene Ausbildung',
		'5'	=> 'abgesschlossenes Studium',
	];

	public static function height()
	{
		$range = range(140, 220);
		$height = [
			null => '-- Keine Angabe --',
		];

		foreach ($range as $value)
		{
			$height[$value] = $value;
		}

		return $height;
	}

}