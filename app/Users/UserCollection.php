<?php namespace Elbsingles\Users;

use Carbon;
use Illuminate\Database\Eloquent\Collection;

class UserCollection extends Collection {

	public function whereGender($gender)
	{
		return $this->filter(function($user) use ($gender)
		{
			return $user->gender == $gender;
		});
	}

	public function lastActive($days)
	{
		return $this->filter(function($user) use ($days)
		{
			return $user->last_active > Carbon::now()->subDays($days);
		});
	}

	public function daysSinceRegister($days)
	{
		return $this->filter(function($user) use ($days)
		{
			return $user->created_at > Carbon::now()->subDays($days);
		});
	}	

	public function whereNot($user)
	{
		return $this->filter(function($userItem) use ($user)
		{
		    return $userItem->id != $user->id;
		});
	}	
}