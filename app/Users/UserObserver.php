<?php namespace Elbsingles\Users;

use Elbsingles\Users\Profiles\Profile;
use Elbsingles\Users\Locations\Location;
use Elbsingles\Users\Settings\Setting;

class UserObserver {

    public function created($user)
    {
        $profile = new Profile;

        $genderInterest = $user->oppositeGender();

        $settings = new Setting([
        	'showPostsInProfile'=> true,
        	'sendDailyMail'		=> true,
            'wantsToFlirt'      => true,
            'genderInterest'    => $genderInterest,
        	]);

        $user->profile()->save($profile);
        $user->settings()->save($settings);
    }

}

