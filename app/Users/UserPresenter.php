<?php namespace Elbsingles\Users;

use Config, HTML;
use Elbsingles\Users\Profiles\ProfileOptions;
use Laracasts\Presenter\Presenter;

class UserPresenter extends Presenter {

	public function thumbnail($class = 'avatar')
	{
		return HTML::image($this->avatar_path('small-'), $this->username, ['class' => $class]);
	}

	public function avatarImg()
	{
		return HTML::image($this->avatar_path(), $this->username, ['class' => 'profile-image']);
	}

	public function profileLink()
	{
		return link_to_route('profile', $this->username, ['username' =>$this->username]);
	}

	public function unreadCount()
	{
		$unread = $this->entity->unreadCount;

		if ($unread > 0)
		{
			return $unread;
		}
	}

	public function height()
	{
		return number_format($this->profile->height / 100, 2, ',', '') . 'm';
	}

	public function attribute($attr)
	{
		if($this->profile->{$attr})
		{
			return ProfileOptions::${$attr}[$this->profile->{$attr}];
		}
	}

	protected function avatar_path($prefix = '')
	{
		if( ! $this->avatar)
		{
			return $this->gender == 'male' ? Config::get('elbseite.default_avatar.male')
										   : Config::get('elbseite.default_avatar.female');
		}

		return '/img/avatars/' . strtolower($this->username) . '/' . $prefix . $this->avatar;
	}
}