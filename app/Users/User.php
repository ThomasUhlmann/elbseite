<?php namespace Elbsingles\Users;

use Auth, Carbon, Config, Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Elbsingles\Messages\Conversation;
use Elbsingles\Services\Notifications\Notification;
use Elbsingles\Users\Roles\RolesTrait;
use Laracasts\Presenter\PresentableTrait;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, PresentableTrait, CanResetPassword, RolesTrait;
	
	/*=============================
	=            Setup            =
	=============================*/

	protected $presenter = 'Elbsingles\Users\UserPresenter';

	protected $fillable = ['username', 'email', 'password', 'gender', 'dob', 'location_id', 'avatar', 'last_active', 'created_at'];

	/* The attributes excluded from the model's JSON form. */
	protected $hidden = ['id', 'dob', 'email', 'password', 'location_id', 'remember_token', 'last_active', 'created_at', 'updated_at', 'pivot'];

	/* The attributes added to the model's JSON form. */
	protected $appends = ['age'];

	/* The User Observer */

	public static function boot()
	{
		parent::boot();

		self::observe(new UserObserver);
	}

	/* Custom Collection */
	
	public function newCollection(array $user = [])
	{
	    return new UserCollection($user);
	}

	/* Dates transformed to Carbon object */
	public function getDates()
	{
	    return ['dob', 'last_active', 'updated_at', 'created_at'];
	}

	/*==========  Getters & Setters  ==========*/
	
	public function setPasswordAttribute($password)
	{
		if (Hash::needsRehash($password))
		{
		    $this->attributes['password'] = Hash::make($password);
		}
		else
		{
			$this->attributes['password'] = $password;
		}
	}

	public function getAgeAttribute()
	{
		return isset($this->dob) ? $this->dob->diffInYears() : '';
	}

	public function getUnreadCountAttribute()
	{
		return $this->conversations()->inbox()->unread()->count();
	}
	
	/*-----  End of Setup  ------*/


	/*==============================================
	=            Database Relationships            =
	==============================================*/
	
	public function profile()
	{
		return $this->hasOne('Elbsingles\Users\Profiles\Profile');
	}

	public function settings()
	{
		return $this->hasOne('Elbsingles\Users\Settings\Setting');
	}

	public function location()
	{
		return $this->belongsTo('Elbsingles\Users\Locations\Location');
	}

	public function posts()
	{
		return $this->hasMany('Elbsingles\Posts\Post');
	}

	public function activities()
	{
		return $this->hasMany('Elbsingles\Activities\Activity');
	}

	public function participates()
	{
		return $this->belongsToMany('Elbsingles\Activities\Activity');
	}

	public function interests()
	{
		return $this->morphToMany('Elbsingles\Posts\Tags\Tag', 'taggable')->with('parent');
	}

	public function notifications()
	{
		return $this->hasMany('Elbsingles\Services\Notifications\Notification');
	}
	
	public function conversations()
	{
		return $this->belongsToMany('Elbsingles\Messages\Conversation', 'user_conversations')
					->with('users')
					->withPivot('trashed', 'archived', 'deleted_at')
					->orderBy('conversations.updated_at', 'desc');
	}

	public function roles()
	{
	    return $this->belongsToMany('Elbsingles\Users\Roles\Role', 'user_roles')->withTimestamps();
	}

	public function hasBlocked()
	{
		return $this->belongsToMany('Elbsingles\Users\User', 'user_blocks', 'user_id', 'blocked_id');
	}

	public function blockedBy()
	{
		return $this->belongsToMany('Elbsingles\Users\User', 'user_blocks', 'blocked_id', 'user_id');
	}

	public function favoritedUsers()
	{
		return $this->morphedByMany('Elbsingles\Users\User', 'favoritable', 'user_favorites');
	}

	public function favoritedPosts()
	{
		return $this->morphedByMany('Elbsingles\Posts\Post', 'favoritable', 'user_favorites');
	}

	public function favoritedActivities()
	{
		return $this->morphedByMany('Elbsingles\Activities\Activity', 'favoritable', 'user_favorites');
	}

	public function favoritedBy()
	{
		return $this->morphToMany('Elbsingles\Users\User', 'favoritable', 'user_favorites');
	}
	
	/*-----  End of Database Relationships  ------*/


    /*====================================
    =            Query Scopes            =
    ====================================*/
    
    public function scopeThatWantToFlirt($query)
    {
        return $query->whereHas('settings', function($q)
        {
            $q->where('wantsToFlirt', true);
        });
    }

    public function scopeWithout($query, User $user)
    {
    	return $query->where('id', '<>', $user->id);
    }

    public function scopelastActive($query, $date)
    {
    	return $query->where('last_active', '>=', $date);
    }

    public function scopeWhereGender($query, $gender)
    {
    	return $query->where('gender', $gender);
    }

    public function scopeMatchSexuality($query)
    {
    	return $query->whereGender(Auth::user()->settings->genderInterest)
					 ->whereHas('settings', function($query)
					   {
					   	   $query->where('genderInterest', Auth::user()->gender);
					   });
    }
    
    /*-----  End of Query Scopes  ------*/
    
    
    /*========================================
    =            Custom Functions            =
    ========================================*/
    
	public function isAuthUser()
	{
		if(Auth::guest()) { return false; }

		return $this->id == Auth::user()->id;
	}

	public function isBlockedBy($user)
	{
		if(Auth::guest() || $this->isAuthUser()) { return false; }

		return $this->blockedBy->contains($user);		
	}

	public function newNotification()
	{
		$notification = new Notification;
		$notification->user()->associate($this);

		return $notification;
	}

    public function wantsToFlirt()
    {
        return $this->settings->wantsToFlirt;
    }

	public function updateLastActive()
	{
		if (Carbon::now()->diffInMinutes($this->last_active) >= Config::get('elbseite.minutes_between_update'))
		{
			$this->update(['last_active' => Carbon::now()]);
		}
	}

	public function oppositeGender()
	{
		return $this->gender == 'male' ? 'female' : 'male';
	}
    
    /*-----  End of Custom Functions  ------*/
}
