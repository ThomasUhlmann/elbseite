<?php namespace Elbsingles\Users\Locations;

use Elbsingles\Services\Geo\Geo;
use Illuminate\Database\Eloquent\Model;

class Location extends Model {

	protected $table = 'user_locations';

    protected $fillable = ['city'];

    public $timestamps = false;

	public static function boot()
	{
		parent::boot();

		self::saving(function($location)
		{
			if($location->city)
			{
				$data = with(new Geo)->fetchLocation($location->city);
		    	$location->city = $data['city'];
		    	$location->latitude = $data['lat'];
		    	$location->longitude = $data['lng'];
			}
		});
	}

    public function __toString()
	{
		return $this->city;
	}

    /*==============================================
    =            Database Relationships            =
    ==============================================*/
    
    public function user()
    {
    	return $this->hasMany('Elbsingles\Users\User');
    }

    /*=================================
    =            Functions            =
    =================================*/
    
	public function distance(Location $location)
	{
		$geo = new Geo();
		$distance = $geo->distance($this->latitude, $this->longitude, $location->latitude, $location->longitude);
		return floor($distance);
	}

}