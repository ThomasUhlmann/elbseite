<?php namespace Elbsingles\Users\Settings;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model {

	protected $table = 'user_settings';

	protected $fillable = [
		'showPostsInProfile',
		'sendDailyMail',
        'wantsToFlirt',
        'genderInterest'
		];


	/**
	 * Database relationships
	 */
	
	public function user()
	{
		return $this->belongsTo('Elbsingles\Users\User');
	}

}