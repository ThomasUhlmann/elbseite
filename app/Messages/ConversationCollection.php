<?php namespace Elbsingles\Messages;

use Illuminate\Database\Eloquent\Collection;

class ConversationCollection extends Collection {

	public function forAjax()
	{
		$response = $this->each(function($conversation){
			$conversation->talkingTo = $conversation->lastMessage()->sendFrom;
			$conversation->preview = substr($conversation->lastMessage()->body, 0, 60);
		});

		return $this->toJson();
	}

	public function showIndex()
	{
		return $this->firstItem() . ' - ' . $this->lastItem() . ' von ' . $this->total();
	}

}