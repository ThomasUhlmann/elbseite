<?php namespace Elbsingles\Messages;

use Auth;
use Elbsingles\Users\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model {

	protected $fillable = ['from_id', 'conversation_id', 'body'];

	protected $touches = ['conversation'];

	public static function boot()
	{
		parent::boot();

		self::created(function($message)
		{
			$message->conversation->update(['last_writer' => $message->from_id]);
		});
	}

	public function setBodyAttribute($value)
	{
		return $this->attributes['body'] = strip_tags($value, '<b><i>');
	}

	/*==============================================
	=            Database Relationships            =
	==============================================*/
	
	public function sendFrom()
	{
		return $this->belongsTo('Elbsingles\Users\User', 'from_id', 'id');
	}

	public function conversation()
	{
		return $this->belongsTo('Elbsingles\Messages\Conversation');
	}

	public function notification()
	{
		return $this->morphOne('Elbsingles\Services\Notifications\Notification', 'notifiable');
	}

	/*====================================
	=            Query Scopes            =
	====================================*/

	public function scopeAfter($query, $date)
	{
		return $query->where('created_at', '>=', $date);
	}

	/*=================================
	=            Functions            =
	=================================*/

	public static function addMessageTo($conversation, $message)
	{
		$message = new static([
			'from_id'		  => Auth::user()->id,
			'body'			  => $message
		]);

		$conversation->messages()->save($message);

		return $message;
	}
	
	public function getSender()
	{
		if( ! $this->from_id )
		{
			$user = new User;
			$user->username = 'Gelöschter Nutzer';
			return $user;
		}

		return $user = $this->sendFrom;
	}

}