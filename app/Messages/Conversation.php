<?php namespace Elbsingles\Messages;

use Auth;
use Elbsingles\Messages\Message;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model {

	/*=============================
	=            Setup            =
	=============================*/

    protected $fillable = ['isRead', 'last_writer', 'archived'];

    public function newCollection(array $conversation = [])
	{
	    return new ConversationCollection($conversation);
	}

	public function getIsReadAttribute($value)
	{
		return $value || $this->last_writer == Auth::user()->id;
	}

	/*===========================================
    =         Database Relationships            =
    ============================================*/

    public function messages()
	{
		return $this->hasMany('Elbsingles\Messages\Message')->with('sendFrom')->orderBy('created_at', 'desc');
	}

    public function users()
	{
		return $this->belongsToMany('Elbsingles\Users\User', 'user_conversations');
	}

	/*=================================
    =         Query Scopes            =
    ===================================*/
    
    public function scopeInbox($query)
	{
		return $query->where('last_writer', '<>', Auth::user()->id)->where('archived', false);
	}

    public function scopeOutbox($query)
	{
		return $query->where('last_writer', Auth::user()->id)->where('archived', false);
	}

	public function scopeArchive($query)
	{
		return $query->where('archived', true);
	}

    public function scopeUnread($query)
	{
		return $query->where('isRead', false);
	}

	public function scopeNotDeleted($query)
	{
		return $query->where('user_conversations.trashed', false);
	}

    public function scopeWithUser($query, $user)
	{
		return $query->whereHas('users', function($query) use ($user) 
							{ $query->where('user_id', $user->id); });
	}

    /*=================================
    =            Functions            =
    =================================*/

    public static function startConversationWith($user)
    {
    	$conversation = self::create(['isRead' => 'false', 'last_writer' => Auth::user()->id]);
		Auth::user()->conversations()->save($conversation);
		$user->conversations()->save($conversation);

		return $conversation;
    }

    public function talkingTo()
    {
    	if( $this->users )
    	{
    		return $this->users->whereNot(Auth::user())->last();
    	}
    }

	public function preview()
	{
		return substr(strip_tags($this->messages->first()->body), 0, 110);
	}

	public function markAsRead()
	{
		if ($this->isRead == false && $this->last_writer != Auth::user()->id) {
			$this->timestamps = false;
			$this->isRead = true;
			$this->save();
		}

		return $this;
	}

	public function markAsUnread()
	{
		if ($this->isRead == true) {
			$this->timestamps = false;
			$this->isRead = false;
			$this->save();
		}

		return $this;
	}

    public function updateLastWriter()
	{
		$this->isRead = false;
		$this->timestamps = true;
		$this->last_writer = Auth::user()->id;
		$this->save();

		return $this;
	}

	public function takeOutOfTrash()
	{
		$users = $this->users()->withPivot('trashed')->get();

		foreach ($users as $user) {
			$user->pivot->trashed = false;
			$user->pivot->save();
		}

		return $this;
	}

	public function takeOutOfArchive()
	{
		$users = $this->users()->withPivot('archived')->get();
		
		foreach ($users as $user) {
			$user->pivot->archived = false;
			$user->pivot->save();
		}

		return $this;
	}
}