<?php

function set_active($path, $active = 'active')
{
	return Request::is($path) ? $active : '';
}

function old_input($value)
{
	return Input::has($value) ? Input::get($value) : null;
}

function avatar_path($user)
{
	if( ! $user->avatar)
	{
		return public_path() . Config::get('elbseite.default_avatar');
	}

	return public_path() . '/img/avatars/' . strtolower($user->username) . '/';
}