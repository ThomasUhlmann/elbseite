<?php namespace Elbsingles\Posts;

use Illuminate\Database\Eloquent\Collection;

class PostCollection extends Collection {

	public function interested($user)
	{
		return $this->filter(function($post) use ($user)
		{
			return $user->interests->contains($post->category);
		});
	}

}