<?php namespace Elbsingles\Posts;

use Carbon;

trait StreamPostTrait {

	public function getId()
	{
		return $this->id;
	}

	public function getType()
	{
		return 'post';
	}

	public function getTypeSpecific()
	{
		return null;
	}

	public function getTags()
	{
		return $this->tags;
	}

	public function getBody()
	{
		return $this->body;
	}

	public function getUser()
	{
		return $this->user;
	}

	public function getFavorited()
	{
		return $this->favorited;
	}

	public function getCanDelete()
	{
		return $this->canDelete;
	}

	public function getPrivateMessaging()
	{
		return $this->privateMessaging;
	}

	public function getComments()
	{
		return $this->comments;
	}

	public function getTimestamp()
	{
		return $this->created_at->timestamp * 1000;
	}
}