<?php namespace Elbsingles\Posts\Tags;

use Illuminate\Database\Eloquent\Collection;

class TagCollection extends Collection {

	public function grabIDs()
	{
		return array_pluck($this->toArray(), 'id');
	}

	public function toSelectList()
	{
		$this->sortForSelectList();
		$result = [];
		foreach ($this->items as $category)
		{
			if ($category->parent != null)
			{
				$result[$category->parent->name][$category->id] = $category->name;
			}
		}

		return $result;
	}

	private function sortForSelectList()
	{
		return $this->sortBy(function($category)
		{
			return $category->parent->name . ' - ' . $category->name;
		});

	}

}