<?php namespace Elbsingles\Posts\Tags;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

	protected $table = 'tags';

	protected $fillable = ['name', 'slug', 'parent_id'];

	protected $visible = ['id', 'name', 'selected'];

	protected $appends = ['selected'];

	public function newCollection(array $tag = [])
	{
	    return new TagCollection($tag);
	}

	public function getSelectedAttribute()
	{
		if(Auth::user())
		{
			return Auth::user()->interests->contains($this->id);
		}
	}

	/*==============================================
	=            Database Relationships            =
	==============================================*/
	
	public function users()
	{
	    return $this->morphedByMany('Elbsingles\Users\User', 'taggable');
	}

	public function posts()
	{
	    return $this->morphedByMany('Elbsingles\Posts\Post', 'taggable');
	}

	public function activites()
	{
	    return $this->morphedByMany('Elbsingles\Activities\Activity', 'taggable');
	}

	public function children()
	{
		return $this->hasMany('Elbsingles\Posts\Tags\Tag', 'parent_id');
	}


	public function parent()
	{
		return $this->belongsTo('Elbsingles\Posts\Tags\Tag', 'parent_id');
	}


	/*====================================
	=            Query Scopes            =
	====================================*/
	
	public function scopeMain($query)
	{
		return $query->where('parent_id', null);
	}

	public function scopeInterestsOf($query, $user)
	{
		$interests = array_pluck($user->interests, 'id');
		return $query->whereIn('id', $interests);
	}

}