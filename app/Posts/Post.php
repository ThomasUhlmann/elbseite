<?php namespace Elbsingles\Posts;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Elbsingles\Posts\Tags\Tag;
use Elbsingles\Services\Stream\StreamFilters;
use Elbsingles\Services\Stream\StreamInterface;

class Post extends Model implements StreamInterface {

	use StreamFilters, StreamPostTrait;

	/*=============================
	=            Setup            =
	=============================*/
	
	protected $fillable = ['body', 'user_id', 'category_id', 'privateMessaging', 'created_at'];

	protected $hidden = ['user_id', 'category_id'];

	protected $appends = ['favorited', 'canDelete'];

	/* Custom Collection */
	
	public function newCollection(array $post = [])
	{
	    return new PostCollection($post);
	}

	/*==========  Getters & Setters  ==========*/

	public function setBodyAttribute($value)
	{
		return $this->attributes['body'] = strip_tags($value, '<b><i>');
	}

	public function getBodyAttribute($value)
	{
		return nl2br($value);
	}

	public function getFavoritedAttribute()
	{
		if(Auth::guest())
		{
			return false;
		}

		return Auth::user()->favoritedPosts->contains($this->id);
	}

	public function getCanDeleteAttribute()
	{
		if(Auth::user()->id == $this->user_id || Auth::user()->hasRole('admin'))
		{
			return true;
		}

		return false;
	}

	public function getPrivateMessagingAttribute($value)
	{
		return (boolean) $value;
	}
	
	/*-----  End of Setup  ------*/

	
	/*==============================================
	=            Database relationships            =
	==============================================*/
	
	public function user()
	{
		return $this->belongsTo('Elbsingles\Users\User');
	}

	public function tags()
	{
		return $this->morphToMany('Elbsingles\Posts\Tags\Tag', 'taggable');
	}

	public function linkable()
	{
	    return $this->morphTo();
	}

	public function comments()
	{
		return $this->morphMany('Elbsingles\Comments\Comment', 'commentable')->oldest();
	}

	public function favoritedBy()
	{
		return $this->morphToMany('Elbsingles\Users\User', 'favoritable', 'favorites');
	}	

}
