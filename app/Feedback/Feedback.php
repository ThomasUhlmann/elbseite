<?php namespace Elbsingles\Feedback;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model {

	protected $table = 'feedback';
	
	protected $fillable = ['user_id', 'message', 'archived'];

	/*==============================================
	=            Database relationships            =
	==============================================*/
	
	public function user()
	{
		return $this->belongsTo('Elbsingles\Users\User');
	}

	/*====================================
	=            Query scopes            =
	====================================*/
	
	public function scopeUnprocessed($query)
	{
		return $query->where('archived', false);
	}
	
	

}