<?php namespace Elbsingles\Comments;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

	/*=============================
	=            Setup            =
	=============================*/

	protected $fillable = ['user_id', 'body', 'commentable_id', 'commentable_type'];

	protected $hidden = ['user_id', 'commentable_id', 'commentable_type', 'created_at', 'updated_at'];

	protected $appends = ['time', 'canDelete'];

	/*==========  Getters & Setters  ==========*/

	public function setBodyAttribute($value)
	{
		return $this->attributes['body'] = strip_tags($value, '<b><i>');
	}

	public function getCanDeleteAttribute()
	{
		if(Auth::user()->id == $this->user_id || Auth::user()->hasRole('admin'))
		{
			return true;
		}

		return false;
	}

	public function getTimeAttribute()
	{
		return $this->created_at->timestamp * 1000;
	}

	/*==============================================
	=            Database Relationships            =
	==============================================*/
	
	public function commentable()
	{
	    return $this->morphTo();
	}

	public function user()
	{
		return $this->belongsTo('Elbsingles\Users\User');
	}


}