<?php namespace Elbsingles\Core\Providers;

use Auth, Carbon, Event, Log;
use Illuminate\Support\ServiceProvider;

class ListenerServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		# E-Mails
		Event::listen('UserHasRegistered', 'Elbsingles\Services\Mailer\UserMailer@welcome');
		Event::listen('FeedbackFormSubmitted', 'Elbsingles\Services\Mailer\AdminMailer@feedback');

		# Notifier
		Event::listen('FriendshipRequested', 'Elbsingles\Services\Notifications\Notifier@friendshipRequest');
		Event::listen('MessageReceived', 'Elbsingles\Services\Notifications\Notifier@messageReceived');

		# Update Last Active
		Event::listen('user.authenticated', function() { Auth::user()->updateLastActive(); });
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		
	}

}
