<?php namespace Elbsingles\Core\Providers;

use Auth, Carbon, View;
use Elbsingles\Activities\Activity;
use Elbsingles\Users\User;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		View::composer('layouts.master', function($view) {

			if (Auth::user()) {
				$view->with('jsonUser', Auth::user()->toJson());
			}
		});
		
		View::composer('widgets.newSingles', function($view)
		{
			$singles = User::thatWantToFlirt()->matchSexuality()->without(Auth::user())->latest('created_at')->limit(4)->get();

			$view->with('singles', $singles);
		});

		View::composer('widgets.upcomingEvents', function($view)
		{
			$events = Activity::upcoming()->oldest('date')->limit(6)->get();

			$view->with('upcomingEvents', $events);
		});

		View::composer('widgets.myActivities', function($view)
		{
			$myActivities = Auth::user()->participates()->where('date', '>=', Carbon::now())->orderBy('date')->get();

			$view->with('myActivities', $myActivities);
		});
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		
	}

}
