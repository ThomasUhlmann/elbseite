<?php namespace Elbsingles\Core\Providers;

use Elbsingles\Users\User;
use Elbsingles\Posts\Category;
use Elbsingles\Posts\Post;
use Elbsingles\Activities\Activity;
use Elbsingles\Comments\Comment;
use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider {

	/**
	 * This namespace is applied to the controller routes in your routes file.
	 *
	 * In addition, it is set as the URL generator's root namespace.
	 *
	 * @var string
	 */
	protected $namespace = 'Elbsingles\Http\Controllers';

	/**
	 * Define your route model bindings, pattern filters, etc.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function boot(Router $router)
	{
		parent::boot($router);

		$router->bind('username', function($value, $route)
		{
			return User::whereUsername($value)->firstOrFail();
		});

		$router->bind('category', function($value, $route)
		{
			return Category::whereSlug($value)->firstOrFail();
		});

		$router->bind('post', function($value, $route)
		{
			return Post::find($value);
		});

		$router->bind('activity', function($value, $route)
		{
			return Activity::find($value);
		});

		$router->bind('comment', function($value, $route)
		{
			return Comment::find($value);
		});
	}

	/**
	 * Define the routes for the application.
	 *
	 * @return void
	 */
	public function map()
	{
		$this->loadRoutesFrom(app_path('Http/routes.php'));
	}

}
