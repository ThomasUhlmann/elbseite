var elixir = require('laravel-elixir');

/*
 |----------------------------------------------------------------
 | Have a Drink!
 |----------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic
 | Gulp tasks for your Laravel application. Elixir supports
 | several common CSS, JavaScript and even testing tools!
 |
 */

elixir(function(mix) {
    mix.sass("main.scss")
       .scriptsIn('resources/assets/angular/', 'public/js/elbseite.js')
       .scripts([
                  'angular-bootstrap/ui-bootstrap-tpls.min.js',
                  'angular-sanitize/angular-sanitize.min.js',
                  'moment/min/moment.min.js',
                  'moment/locale/de.js',
                  'angular-moment/angular-moment.min.js',
       		'ngInfiniteScroll/build/ng-infinite-scroll.min.js',
                  'ngSmoothScroll/angular-smooth-scroll.min.js',
                  'angular-bootstrap-datetimepicker/src/js/datetimepicker.js',
                  'angular-elastic/elastic.js',
            ],
            'vendor/bower_components/',
            'public/js/dependencies.min.js')
       .copy(
                  'resources/assets/fonts',
                  'public/fonts'
            )
       .publish(
                  'angular/angular.min.js',
                  'public/js/vendor/angular.min.js'
            )
       .publish(
                  'angular/angular.min.js.map',
                  'public/js/vendor/angular.min.js.map'
            )
       .publish(
                  'angular-moment/angular-moment.min.js.map',
                  'public/js/angular-moment.min.js.map'
            )
});