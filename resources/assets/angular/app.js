(function() {

	var app = angular.module('elbseite', [
		'infinite-scroll',
		'smoothScroll',
		'checklist-model',
		'ngSanitize',
		'ui.bootstrap',
		'ui.bootstrap.datetimepicker',
		'angularMoment',
		'monospaced.elastic',
	]);

	app.config(['$httpProvider', function ($httpProvider) {
		$httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
	}]);
	
	app.run(['amMoment', function(amMoment) {
			amMoment.changeLocale('de');
		}]);
}());
