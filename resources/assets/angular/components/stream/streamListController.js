(function() {

	var streamListController = ['$scope', '$http', 'filterFilter', 'streamService', function($scope, $http, filterFilter, streamService) {
	
			$scope.interests = [];
			$scope.posts = {};
			$scope.busy = false;
			$scope.endReached = false;
	
			$scope.$watch( function() { return streamService.posts(); }, function() {
				$scope.posts = streamService.posts();
			});
	
			$scope.$watch( function() { return streamService.interests(); }, function() {
				$scope.interests = streamService.interests();
			});
	
			$scope.$watch( function() { return streamService.busy(); }, function() {
				$scope.busy = streamService.busy();
			});
	
			$scope.$watch( function() { return streamService.endReached(); }, function() {
				$scope.endReached = streamService.endReached();
			});
	
			$scope.fetchPosts = function(query) {
	
				streamService.fetchPosts(query);
			}
	
			$scope.submitPost = function() {
	
				streamService.storePost($scope.postData);
				$scope.postData = {};
			}
	
		}];

	angular.module('elbseite').controller('StreamListController', streamListController);

}());