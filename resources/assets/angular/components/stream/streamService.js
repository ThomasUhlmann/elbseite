(function() {

	var streamService = ['$http', 'authUser', function($http, authUser) {
	
			var posts = [],
				busy = false,
				endReached = false,
				nextPageUrl = null,
				interests = [];
	
				$http.get('/api/interests')
					.success(function(data) {
						interests = data;
					});
	
			return {
				posts: function() { return posts; },
				busy: function() { return busy; },
				endReached: function() { return endReached; },
				interests: function() { return interests; },
				nextPageUrl: function() { return nextPageUrl; },
	
				fetchPosts: function(query, reset) {
	
					if(busy || endReached) return; 
					if(reset) posts = [];
					url = nextPageUrl || '/api/stream/?' + $.param(query || '');
	
					busy = true;
	
					$http.get(url)
						.success(function(data) {
							for (var i = 0; i < data.posts.length; i++) {
								posts.push(data.posts[i]);
							};
							nextPageUrl = data.nextPageUrl;
							if(nextPageUrl == null) endReached = true;
							busy = false;
							console.log('New push');
							if (data.posts.length) {
								console.log('new posts');
							}
							else {
								console.log('no posts');
							}

						});
				},
	
				deletePost: function(index) {
	
					$http.delete('/api/post/' + posts[index].id + '/delete')
						.success(function() {
	
							posts.splice(index, 1);
						});
				},
	
				setUrl: function(url) {
					nextPageUrl = url;
					endReached = false;
				},
	
				storePost: function(postData) {
					
					$http.post('/api/post/store', postData)
						.success(function(data) {
							posts.unshift(data.newPost[0]);
						})
				},
	
				deselectAll: function() {
	
					interests = angular.copy(interests.map(function(interest) {
						interest.selected = false;
						return interest;
					}));
				},
	
				selectAll: function() {
	
					interests = angular.copy(interests.map(function(interest) {
						interest.selected = true
						return interest;
					}));
				}
			}
		}];

	angular.module('elbseite').factory('streamService', streamService);

}());