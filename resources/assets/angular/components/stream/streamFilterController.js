(function() {

	var streamFilterController = ['$scope', 'filterFilter', 'streamService', function($scope, filterFilter, streamService) {
	
			$scope.interests = streamService.interests();
			$scope.showInterests = false;
			$scope.selected = [];
	
			$scope.$watch( function() { return streamService.interests(); }, function () {
				$scope.interests = streamService.interests();
				$scope.selected = filterFilter($scope.interests, {selected: true}).map(function(interest) { return interest.id });
			});
	
			$scope.update = function() {
	
				streamService.setUrl(null);
				streamService.fetchPosts({
						tags: $scope.selected
					}, true);
			};
	
			$scope.toggleInterests = function() {
	
				$scope.showInterests = ! $scope.showInterests;
			};
	
			$scope.deselectAll = function() {
	
				streamService.deselectAll();
			};
	
			$scope.selectAll = function() {
	
				streamService.selectAll();
			};
		}];

	angular.module('elbseite').controller('StreamFilterController', streamFilterController);

}());