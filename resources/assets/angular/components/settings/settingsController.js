(function() {

	var settingsController = ['$scope', function($scope) {

			var activeTab = 1;

			$scope.isSelected = function(tab) {
				return tab == activeTab;
			}

			$scope.selectTab = function(tab) {
				activeTab = tab;
			}
	
		}];

	angular.module('elbseite').controller('SettingsController', settingsController);

}());