(function() {

	var messagesController = ['$scope', '$http', '$window', 'modalService', function($scope, $http, $window, modalService) {
	
			$scope.messages = [];
			$scope.busy = true;
			var nextPageUrl = '';
	
			$scope.$watch('username', function(username) {
				$http.get('/api/messages/' + username)
					.success(function(data) {
						$scope.messages = data.messages;
						nextPageUrl = data.nextPageUrl;
						$scope.busy = false;
					});
			});
	
			$scope.moreMessages = function() {
	
				if($scope.busy || nextPageUrl == null) return; 
				$scope.busy = true;
	
				$http.get(nextPageUrl)
					.success(function(data) {
						for (var i = 0; i < data.messages.length; i++) {
							$scope.messages.push(data.messages[i]);
						};
						nextPageUrl = data.nextPageUrl;
						$scope.busy = false;
					});
			}
	
			$scope.postNewMessage = function() {
	
				$http.post('/api/messages/' + $scope.username + '/store', $scope.newMessage)
					.success(function(data) {
						$scope.messages.unshift(data.newMessage[0]);
						$scope.newMessage = {};
					});
			}
	
			$scope.deleteConversation = function(username) {
	
				var modalOptions = {
					closeButtonText: 'Abbrechen',
					actionButtonText: 'Nachrichten löschen',
					bodyText: 'Diese Nachrichten wirklich löschen?'
				};
	
				modalService.showModal({size: 'sm'}, modalOptions)
							.then(function (result) {
								$http.post('/api/messages/' + username + '/delete')
									.success(function() {
										$window.location.replace('/nachrichten');
									});
							});
			}
		}];

	angular.module('elbseite').controller('MessagesController', messagesController);

}());