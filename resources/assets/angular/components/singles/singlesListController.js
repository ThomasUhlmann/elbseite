(function() {

	var singlesListController = ['$scope', '$http', 'singlesService', function($scope, $http, singlesService) {
	
			$scope.singles = {};
			$scope.ageSpan = {};
	
			$scope.$watchGroup([
				function() { return singlesService.singles(); },
				function() { return singlesService.ageSpan(); },
			], function() {
				$scope.singles = singlesService.singles();
				$scope.ageSpan = singlesService.ageSpan();
			});
		}];

	angular.module('elbseite').controller('SinglesListController', singlesListController);

}());