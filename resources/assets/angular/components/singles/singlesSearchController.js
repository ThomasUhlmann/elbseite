(function() {

	var singlesSearchController = ['$scope', '$http', 'singlesService', function($scope, $http, singlesService) {
	
			$scope.genders = [
				{label: 'Männer', value: 'male'},
				{label: 'Frauen', value: 'female'}
			];
	
			$scope.$watch( function() { return singlesService.ageSpan(); }, function() {
				$scope.ageSpan = singlesService.ageSpan();
			});
		}];

	angular.module('elbseite').controller('SinglesSearchController', singlesSearchController);

}());