(function() {

	var singlesService = ['$http', function($http) {
	
			var singles = {},
				ageSpan = {};
	
			$http.get('/api/singles/').success(function(data) {
				singles = data.singles;
				ageSpan = {
					begin: (data.user.age - 8) >= 18 ? data.user.age - 8 : 18,
					end: data.user.age + 8
				};
			});
	
			return {
				singles: function() { return singles; },
				ageSpan: function() { return ageSpan },
			}
		}];

	angular.module('elbseite').factory('singlesService', singlesService);

}());