(function() {

	var loginController = ['$scope', '$http', '$window', function($scope, $http, $window) {
	
			$scope.inputData = {
				remember: true
			};
			$scope.message = '';
	
			$scope.login = function() {
	
				if ($scope.message) {
					$scope.message = 'Login wird durchgeführt.'
				};
	
				$http.post('/login', $scope.inputData)
					.success(function(data) {
						if (data.login == true)
						{
							$window.location.replace('/');
						}
						else
						{
							$scope.message = 'Die Eingabe war nicht richtig.';
							$scope.inputData.password = '';
						}
					});
			}
	
		}];

	angular.module('elbseite').controller('LoginController', loginController);

}());