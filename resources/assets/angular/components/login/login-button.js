(function () {

	var loginButton = function() {

		var loginButtonController = ['$scope', '$http', '$attrs', 'modalService', function($scope, $http, $attrs, modalService) {
		
					$scope.open = function() {
		
						var modalOptions = {
								closeButtonText: 'Abbrechen',
								actionButtonShow: false,
								actionButtonText: 'Hey',
								template: 'templates/login/form.html'
							};
		
							modalService.showModal({size: 'sm'}, modalOptions)
					}
				}];

		return {
				controller: loginButtonController,
				scope: true,
			};
	}

	angular.module('elbseite').directive('loginButton', loginButton);

}());