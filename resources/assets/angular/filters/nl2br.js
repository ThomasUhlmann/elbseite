(function() {

	var nl2br = function() {
		return function(text) {
			return text.replace(/\n/g, '<br/>');
		}
  	};

	angular.module('elbseite').filter('nl2br', nl2br);
}());