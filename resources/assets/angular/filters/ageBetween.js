(function() {

  var ageBetween = function () {
    return function (singles, arg1, arg2) {

      var ageBegin = arg1 || 18,
          ageEnd = arg2 || 120,
          filtered = [];
      
      for (var i = 0; i < singles.length; i++) {
        var single = singles[i];

        if (single.age >= ageBegin && single.age <= ageEnd) {
          filtered.push(single);
        }
      }
      return filtered;
    };
  }

  angular.module('elbseite').filter('ageBetween', ageBetween);

}());
