(function () {

	var scrollToTop = ['$window', 'smoothScroll', function($window, smoothScroll) {

		var link = function(scope, element, attrs) {

			scope.visible = false;

			angular.element($window).bind('scroll', function() {
				if (this.pageYOffset >= 100) {
					scope.visible = true;
				} else {
					scope.visible = false;
				}
				scope.$apply();
			});

			scope.scroll = function() {
				var options = attrs;
				var body = document.getElementById('body');

				smoothScroll(body, options);
			}
		}

		return {
				templateUrl: '/templates/shared/scrollToTop.html',
				link: link,
				replace: true
			};
	}];

	angular.module('elbseite').directive('scrollToTop', scrollToTop);

}());