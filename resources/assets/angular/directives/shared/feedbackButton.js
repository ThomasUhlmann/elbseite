(function () {

	var feedbackButton = function() {

		var feedbackModalController = ['$scope', '$http', '$modalInstance', function($scope, $http, $modalInstance) {
		
					$scope.feedback = {
						message: ''
					};
		
					$scope.send = function() {
		
						$http.post('/api/feedback/store', $scope.feedback)
								.success(function(reponse) {
									$modalInstance.dismiss('cancel');
								});
					}
				}];

		var feedbackButtonController = ['$scope', '$http', '$attrs', 'modalService', function($scope, $http, $attrs, modalService) {
		
					$scope.open = function() {
		
						var modalOptions = {
								closeButtonText: 'Abbrechen',
								actionButtonText: 'Feedback senden',
								actionButtonShow: true,
							};
		
						modalService.showModal({
							controller: feedbackModalController,
							templateUrl: '/templates/shared/feedbackBody.html',
						}, modalOptions)
									.then(function (result) {
		
									});
					}
				}];

		return {
				templateUrl: '/templates/shared/feedbackButton.html',
				controller: feedbackButtonController,
				scope: true,
				replace: true
			};
	}

	angular.module('elbseite').directive('feedbackButton', feedbackButton);

}());