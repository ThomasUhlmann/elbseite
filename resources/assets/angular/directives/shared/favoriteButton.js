(function () {

	var favoriteButton = function() {

		var favoriteButtonController = ['$scope', '$http', '$attrs', function($scope, $http, $attrs) {
					
					$scope.buttonClass = getButtonClass($attrs.favorited);
		
					$scope.$watch($attrs.favorited, function (newval, oldval) {
		                if (newval) {
							$scope.buttonClass = getButtonClass(newval);
		                }
		            });
					
					$scope.rememberThis = function() {
		
						var url = '/api/favorites/' + $attrs.item + '/remember?type=' + $scope.post.type;
		
						$http.post(url)
							.success(function(data) {
								$scope.buttonClass = getButtonClass(data.favorited);
							});
					}
		
					function getButtonClass (favorited) {
						return favorited == true ? 'icon-star-two active' : 'icon-star';
					}
				}];

		return {
				templateUrl: 'templates/shared/favoriteButton.html',
				controller: favoriteButtonController,
			};
	}

	angular.module('elbseite').directive('favoriteButton', favoriteButton);

}());