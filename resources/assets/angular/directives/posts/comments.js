(function() {

	var comments = function() {

		var commentsController = ['$scope', '$http', '$timeout', 'modalService', function($scope, $http, $timeout, modalService) {
		
					$scope.comments = $scope.post.comments;
					$scope.limit = -3;
					$scope.expandClass = 'icon-chevron-down-circle';
					$scope.showAll = false;
					$scope.isCreating = false;
					$scope.isActive = false;
		
					$scope.submitComment = function() {
		
						$http.post('/api/' + $scope.post.type + '/' + $scope.post.id + '/comments/store', $scope.commentData)
							.success(function(newComment) {
								$scope.comments.push(newComment);
								$scope.commentData = {};
								$scope.isCreating = false;
								$scope.isActive = false;
							})
					}
		
					$scope.deleteComment = function(index) {
		
						var modalOptions = {
								closeButtonText: 'Abbrechen',
								actionButtonText: 'Kommentar löschen',
								bodyText: 'Diesen Kommentar wirklich löschen?'
							};
		
							modalService.showModal({size: 'sm'}, modalOptions)
										.then(function (result) {
											$http.delete('/api/' + $scope.post.type + '/' + $scope.post.id + '/comments/' + $scope.comments[index].id + '/delete')
												.success(function() {
													$scope.comments.splice(index, 1);
												});
										});
		
					}
		
					$scope.toggleComments = function() {

						if($scope.comments.length <= 3) return;
		
						if ($scope.limit === 'Infinity')
						{
							$scope.limit = -3;
							$scope.showAll = false;
						}
						else
						{
							$scope.limit = 'Infinity';
							$scope.showAll = true;
						}
					}
		
					$scope.canExpandComments = function() {
		
						return $scope.comments.length >= 4;
					}
		
					$scope.startCreating = function() {
		
						$scope.isCreating = true;
						$scope.isActive = true;
						$timeout(function() {
							$('textarea.active').focus();
						});
					}
		
					$scope.unfocus = function() {
						$scope.isActive = false;
					}
		
					$scope.stopCreating = function() {
		
						$scope.isCreating = false;
					}
		
					$scope.$watch('showAll', function(newval, oldval) {
						if (newval == true)
						{
							$scope.expandClass = 'icon-chevron-up-circle';
						}
						else
						{
							$scope.expandClass = 'icon-chevron-down-circle';
						}
					});
				}];

		return {
			templateUrl: 'templates/posts/comments.html',
			controller: commentsController
		};
	}

	angular.module('elbseite').directive('comments', comments);

}());