(function() {

	var postTemplate = function() {

		var postController = ['$scope', 'streamService', 'modalService', function($scope, streamService, modalService) {
				
						$scope.template = '/templates/posts/content/' + $scope.post.type + '.html'
		
						$scope.isLinked = function() {
		
							return $scope.post.type != 'post';
						}
		
						$scope.deletePost = function(index) {
		
							var modalOptions = {
								closeButtonText: 'Abbrechen',
								actionButtonText: 'Beitrag löschen',
								bodyText: 'Diesen Beitrag wirklich löschen?'
							};
		
							modalService.showModal({size: 'sm'}, modalOptions)
										.then(function (result) {
											streamService.deletePost(index);
										});
						}
					}];
		
		return {
			templateUrl: '/templates/posts/post.html',
			controller: postController
		};
	}

	angular.module('elbseite').directive('post', postTemplate);

}());