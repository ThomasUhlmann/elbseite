(function () {

	var blockButton = function() {

		var blockButtonController = ['$scope', '$http', '$attrs', function($scope, $http, $attrs) {
		
					$scope.btnClass = $attrs.blocked ? 'active' : '';
		
					$scope.block = function() {
		
						$http.post('/api/user/' + $attrs.username + '/block')
							.success(function(data) {
								$scope.btnClass = data.blocked ? 'active' : '';
							});
					}
				}];

		return {
				template: '<div type="button" class="block-button" ng-class="btnClass" ng-click="block()"><div class="icon-denied-block"></div></div>',
				controller: blockButtonController
			};
	}

	angular.module('elbseite').directive('blockButton', blockButton);

}());