(function() {

	var avatarTemplate = ['authUser', function(authUser) {

				var link = function ($scope, $element, $attrs) {
					
					$scope.$watch($attrs.context, function(context) {
	
						if ((context !== null) && (context !== undefined) && (context !== '')) {
	
							var size = $attrs.size || 40;

							if (context == 'authUser') {
								var user = authUser;
							}
							else if (context == 'single') {
								var user = $scope.single;
							}
							else
							{
								var user = $scope[context].user;
							}
	
							img = '<img src="' + avatarUrl(user) + '" class="avatar" width="' + size + '">';
	
							$element.append('<a href="/@' + user.username + '">' + img + '</a>');
						}
					})
	
					avatarUrl = function(user) {
		
						if(user.avatar) {
							return '/img/avatars/' + user.username + '/' + user.avatar;
						}
						return '/img/avatars/default-' + user.gender + '.png';
					}
				};
	
			return {
				restrict: 'E',
				link: link
			};
		}];

	angular.module('elbseite').directive('avatar', avatarTemplate);

}());