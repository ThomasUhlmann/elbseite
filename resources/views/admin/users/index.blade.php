@extends('layouts.admin')

@section('content')

	<h1>Teilnehmer suchen</h1>
	<hr>
	<table id="users" class="table table-striped table-bordered dataTable">
	</table>

@stop

@section('scripts')
    <script src="//cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/plug-ins/a5734b29083/integration/bootstrap/3/dataTables.bootstrap.js"></script>
	<script>
		$(document).ready(function() {

			$("#users").DataTable({
				"aLengthMenu": [[15, 30, -1], [15, 30, "Alle"]],
				"bStateSave": true,
				"bProcessing": true,
		        "processing": true,
		        "ajax": "/admin/users",
				"columns": [
					{ "data" : "username",	"title" : "Benutzername",	"orderable": true, "searchable": true },
					{ "data" : "dob",		"title" : "Alter",			"orderable": true, "searchable": true },
					{ "data" : "gender",	"title" : "Geschlecht",		"orderable": true, "searchable": true },
					{ "data" : "email",		"title" : "Email",			"orderable": true, "searchable": true },
					{ "data" : "name",		"title" : "Rolle",			"orderable": true, "searchable": true },
				],
				"columnDefs": [ { //this prevents errors if the data is null
				    "targets": "_all",
				    "defaultContent": ""
				} ],
		        "language": {
					"lengthMenu": "_MENU_ Einträge pro Seite",
					"zeroRecords": "Keine Resultate",
					"info": "Seite _PAGE_ von _PAGES_ (insgesamt _MAX_ Einträgen)",
					"infoEmpty": "Die Suche ergab keinen Treffer",
					"infoFiltered": "(insgesamt _MAX_ Einträgen)",
					"search": "Suche",
				},
			});

		} );
	</script>

@stop