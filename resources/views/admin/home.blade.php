@extends('layouts.admin')

@section('content')
	<div class="row">
		<div class="col-md-4">
			@include('widgets.admin.activeUsers')
		</div>
		<div class="col-md-8">
			@include('admin.feedback.index')
		</div>
	</div>
@stop