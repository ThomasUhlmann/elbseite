<div class="panel stacked">
	<div class="panel-heading">
		<span class="panel-icon icon-star"></span>
		<h3 class="panel-title">Feedback</h3>
	</div>
	<table class="table">
		<thead>
			<tr>
				<th>Nutzer</th>
				<th>Nachricht</th>
				<th>Optionen</th>
			</tr>
		</thead>
		<tbody>
			@foreach($feedbackCollection as $feedback)
				<tr>
					<td>
						<avatar user="{{ $feedback->user->toJson() }}" width="50"></avatar> 
						<a href="{{ '@' . $feedback->user->username}}">{{ $feedback->user->username }}</a>
					<td>
						<b>Vom: {{ $feedback->created_at->format('d.m.Y') }}</b>
						<div class="clearfix"></div>
						{{ $feedback->message }}
					</td>
					<td>
						{!! Form::open(['route' => ['feedback.delete', $feedback], 'method' => 'DELETE']) !!}
							<button type="submit"><span class="icon-trash"></span></button>
						{!! Form::close() !!}
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>