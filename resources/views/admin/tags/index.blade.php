@extends('layouts.admin')

@section('content')
	<table class="table">
		<thead>
			<tr>
				<th>Name</th>
				<th>Slug</th>
				<th>Posts</th>
				<th>Oberkategorie</th>
				<th>Optionen</th>
			</tr>
		</thead>
		<tbody>
			@foreach($tags as $tag)
				<tr>
					<td>{{ $tag->name }}</td>
					<td>{{ $tag->slug }}</td>
					<td>{{ $tag->posts()->count() }}</td>
					<td></td>
					<td>{!! link_to_route('admin.tags.edit', 'Bearbeiten', [$tag->slug]) !!}</td>
				</tr>
				@foreach($tag->children as $subtags)
					<tr>
						<td>{{ $subtags->name }}</td>
						<td>{{ $subtags->slug }}</td>
						<td>{{ $subtags->posts()->count() }}</td>
						<td>{{ $tag->name }}</td>
						<td>
							{!! link_to_route('admin.tags.edit', 'Bearbeiten', [$subtags->slug]) !!}
						</td>
					</tr>
				@endforeach
			@endforeach
		</tbody>
	</table>

	{!! link_to_route('admin.tags.create', 'Tags erstellen', [], ['class' => 'btn btn-success']) !!}
@stop