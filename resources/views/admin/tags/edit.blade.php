@extends('layouts.admin')

@section('content')
	
	<h1>Kategorie bearbeiten</h1>

	{!! Form::model($category, ['route' => ['admin.category.update', $category->slug], 'method' => 'put']) !!}
		
		@include('admin.category.partials.form')

	{!! Form::close() !!}

	{!! Form::open(['route' => ['admin.category.destroy', $category->name], 'method' => 'delete']) !!}
		
		{!! Form::submit('Löschen', ['class' => 'btn btn-link pull-right']) !!}

	{!! Form::close() !!}

@stop