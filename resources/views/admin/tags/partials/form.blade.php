<div class="form-group">
	{!! Form::label('name', 'Kategorie') !!}
	{!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('slug', 'Slug') !!}
	{!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('parent_id', 'Oberkategorie') !!}
	{!! Form::select('parent_id', $categories, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::submit('Speichern', ['class' => 'btn btn-primary']) !!}
</div>