<div class="comments">
	@foreach($post->comments as $comment)
		<div class="user">
			{!! $comment->user->present()->thumbnail !!}
			<div class="wrapper">
				{!! $comment->user->present()->profileLink !!}
				<span class="post-date">gepostet am {{ $comment->created_at->format('d.m.Y') }}</span>
			</div>
		</div>
		<div class="body">
			{{ $comment->body }}
		</div>
	@endforeach

	{!! Form::open(['route' => ['comment.store', $post->id]]) !!}

		<div class="create">
			<div class="user">
				{!! Auth::user()->present()->thumbnail !!}
			</div>

			<div class="wrapper">
				<div class="form-group">
					{!! Form::textarea('body', null, ['class' => 'form-control', 'rows' => 3]) !!}
				</div>
				{!! Form::submit('Abschicken', ['class' => 'btn btn-primary']) !!}
			</div>

		</div>
	


	{!! Form::close() !!}
</div>