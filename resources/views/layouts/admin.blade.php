@extends('layouts.design')

@section('css')
	<link href="//cdn.datatables.net/plug-ins/a5734b29083/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">
@stop

@section('links')
	  <li>{!! link_to_route('users', 'Nutzer') !!}</li>
	  <li>{!! link_to_route('admin.tags.index', 'Tags') !!}</li>
@stop