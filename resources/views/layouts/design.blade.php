<!DOCTYPE html>
<html lang="de" ng-app="elbseite">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ Config::get('elbseite.page_name') }}</title>

        <!-- Bootstrap -->
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
        @yield('css')
        <link href="/css/main.css" rel="stylesheet">

        <!-- Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600' rel='stylesheet' type='text/css'>

    </head>
<body id="body">
    <div class="main-wrapper">

        <div class="push-navbar"></div>
        <!-- Navbar -->
        @include('layouts.partials.navbar')

        <!-- Billboard -->
        @yield('header')

        <!-- Main Content -->
        <div class="container" id="main">
            @yield('content')
        </div>

        <div class="push"></div>
    </div>

    <!-- Footer -->
    <footer>
        <div class="container">
            <span>
                &copy; {{ Carbon::now()->year }} {{ Config::get('elbseite.page_name') }}
            </span>
            <span class="pull-right">
                {!! link_to_route('impressum', 'Impressum') !!}
            </span>
        </div>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.8/angular.min.js"></script>
    <script src="/js/dependencies.min.js"></script>
    <script src="/js/elbseite.js"></script>
    @yield('scripts')
  </body>
</html>