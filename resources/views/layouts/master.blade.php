@extends('layouts.design')

@section('header')
	<!-- Scroll To Top -->
	<div scroll-to-top></div>

	<!-- Button trigger modal -->
	<div feedback-button></div>
@stop

@section('links')
	@if(Auth::check())

	    @if(Auth::user()->wantsToFlirt())
	        <li class="move">
	        	<a href="{{ URL::route('singles.index') 	}}">
	        		<div class="icon icon-heart"></div>
					<span>Anmachen</span>
	        	</a>
	        </li>
	    @endif

		@if(Auth::user()->notifications->count())
			<li>{!! link_to_route('profile', 'Notifications ' . Auth::user()->notifications->count(), ['username' => Auth::user()->username]) !!}</li>
		@endif

		<li class="move">
			<a href="{{ URL::route('events.index') }}">
				<div class="icon icon-calendar"></div>
				<span>Mitmachen</span>
			</a>
		</li>
		
		<li class="move">
			<a href="{{ URL::route('conversation.inbox') }}">
				<div class="icon icon-mail"></div>
				<div class="badge progress-bar-danger">{{ Auth::user()->present()->unreadCount }}</div>
				<span>Post</span>
			</a>
		</li>


		<li class="dropdown" dropdown>
			<a href="" class="dropdown-toggle user" dropdown-toggle>
				<img src="{{ Auth::user()->present()->avatar_path }}">
				<span class="icon-caret-down-two"></span>
			</a>
			<ul class="dropdown-menu" role="menu">
				@if(Auth::user()->hasRole('admin'))
					<li>{!! link_to_route('admin', 'Admin', []) !!}</li>
				@endif
				<li>{!! link_to_route('profile', 'Profil', ['username' => Auth::user()->username]) !!}</li>
				<li>{!! link_to_route('favorite.index', 'Spickzettel') !!}</li>
				<li>{!! link_to_route('profile.edit', 'Einstellungen', ['username' => Auth::user()->username]) !!}</li>
				<li>{!! link_to_route('help', 'Hilfe') !!}</li>
				<li class="divider"></li>
				<li>{!! link_to_route('logout', 'Logout', []) !!}</li>
			</ul>
		</li>
	@else
	  <li>{!! link_to_route('register', 'Anmelden') !!}</li>
	  <li><a login-button ng-click="open()">Login</a></li>
	@endif
@stop

@section('scripts')
	<script>
		angular.module('elbseite').value('authUser', {!! $jsonUser or "''" !!});
    </script>
@stop