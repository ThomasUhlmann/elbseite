<div class="">
	
	<form class="form" ng-submit="submitPost()">
		<div class="form-group">
			<label for="">Kategorie</label>
			<select class="form-control" ng-options="interest.name for interest in (interests | filter:{selected: 'true'} ) track by interest.id" ng-model="postData.category" required>
				<option value="">---- Kategorie auswählen ----</option>
			</select>
		</div>
		<div class="form-group">
			<label for="">Text</label>
			<textarea msd-elastic ng-model="postData.body" rows="4" class="form-control" required></textarea>	
		</div>

		<button type="submit" class="btn btn-blue">Abschicken</button>
	</form>

</div>