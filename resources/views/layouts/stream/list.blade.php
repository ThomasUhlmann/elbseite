<div infinite-scroll="fetchPosts()" infinite-scroll-disabled="busy" infinite-scroll-distance="0" ng-init="fetchPosts({{ $query }})">
	<article data-post class="entry" ng-repeat="post in posts" ng-class="post.type">
	</article>
</div>
<div class="ajax-loader" ng-show="busy"></div>
<div ng-show="posts.length == 0 && !busy">Keine Posts vorhanden</div>
<div class="the-end" ng-if="endReached && posts.length > 0"></div>