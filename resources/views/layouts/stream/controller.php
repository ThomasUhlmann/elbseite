<div class="" ng-controller="StreamFilterController">

	<div ng-click="toggleInterests()"><span class="icon-filter"></span> Nach Interessen filtern</div>

	<div ng-cloak>
		<div ng-repeat="interest in interests" ng-show="showInterests">
			<input type="checkbox" checklist-model="selected" checklist-value="interest.id"> {{ interest.name }}
		</div>
		
		<button class="btn btn-blue" ng-click="update()" ng-show="showInterests">Update</button>

		<button class="btn btn-warning" ng-click="deselectAll()" ng-show="showInterests">Deselect</button>

		<button class="btn btn-warning" ng-click="selectAll()" ng-show="showInterests">All</button>
	</div>

</div>