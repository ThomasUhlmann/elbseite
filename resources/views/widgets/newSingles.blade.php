@if($singles)
	<h3>Neu eingetroffen</h3>
	<div class="clearfix">	</div>
	<ul class="list-singles">
		@foreach($singles as $single)
			<li>
				<a href="/{{ '@' . $single->username }}">
					{!! $single->present()->thumbnail !!}
				</a>
				<div class="wrapper">
					<a href="/{{ '@' . $single->username }}">{{ $single->username }}</a>
					<span>Alter: {{ $single->age }}</span>
				</div>
			</li>
		@endforeach
	</ul>
	{!! link_to_route('singles.index', 'Mehr davon', null, ['class' => 'btn btn-red']) !!}
	<div class="clearfix"></div>
@endif
