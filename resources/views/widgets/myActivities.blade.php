@if( ! $myActivities->isEmpty())
	<h3>Hier bin ich dabei</h3>
	<div class="clearfix"></div>
	@foreach($myActivities as $activity)
		<ul class="list-events">
			<li>
				{{ $activity->date->format('d.m.Y') }}
				{{ $activity->title }}
			</li>
		</ul>
	@endforeach
@endif
