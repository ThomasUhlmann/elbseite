@if($users)
	<div class="panel stacked">
		<div class="panel-heading">
			<span class="panel-icon icon-star"></span>
			<h3 class="panel-title">Aktive Nutzer</h3>
		</div>
		<table class="table">
			<thead>
				<tr>
					<th>Benutzer</th>
					<th>Frauen</th>
					<th>Männer</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Aktiv <small>(diese Woche)</small></td>
					<td align="center">{{ $users->whereGender('female')->lastActive(7)->count() }}</td>
					<td align="center">{{ $users->whereGender('male')->lastActive(7)->count() }}</td>
				</tr>
				<tr >
					<td>Neu <small>(diese Woche)</small></td>
					<td align="center">{{ $users->whereGender('female')->daysSinceRegister(7)->count() }}</td>
					<td align="center">{{ $users->whereGender('male')->daysSinceRegister(7)->count() }}</td>
				</tr>
				<tr>
					<td>Gesamt</td>
					<td align="center">{{ $users->whereGender('female')->count() }}</td>
					<td align="center">{{ $users->whereGender('male')->count() }}</td>
				</tr>
			</tbody>
		</table>
	</div>
@endif