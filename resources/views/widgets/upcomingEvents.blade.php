<h3>Demnächst</h3>
<div class="clearfix"></div>
@foreach($upcomingEvents as $event)
	<ul class="list-events">
		<li>
			{{ $event->date->format('d.m.Y') }}
			{{ $event->title }}
		</li>
	</ul>
@endforeach
{!! link_to_route('events.index', 'Geht da noch mehr?', null, ['class' => 'btn btn-red']) !!}
