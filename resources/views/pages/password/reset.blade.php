@extends('layouts.master')

@section('content')
	<h1>Passwort ändern</h1>
	
	@if(Session::has('error'))
		{{ Session::get('error') }}
	@endif

	{!! Form::open(['route' => 'password.reset.store']) !!}
		
		{!! Form::hidden('token', $token) !!}

		<div class="form-group">
			{!! Form::label('email', 'E-Mail') !!}
			{!! Form::text('email', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('password', 'Neues Passwort') !!}
			{!! Form::password('password', ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('password_confirmation', 'Passwort bestätigen') !!}
			{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
		</div>
		
		<div class="form-group">
			{!! Form::submit('Passwort ändern', ['class' => 'btn btn-primary']) !!}
		</div>

	{!! Form::close() !!}

@stop