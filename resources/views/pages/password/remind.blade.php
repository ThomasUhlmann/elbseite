@extends('layouts.master')

@section('content')
	<h1>Passwort zusenden</h1>

	{!! Form::open(['route' => 'password.remind.mail']) !!}
		
		<div class="form-group">
			{!! Form::label('email', 'E-Mail') !!}
			{!! Form::text('email', null, ['class' => 'form-control']) !!}
		</div>
		
		<div class="form-group">
			{!! Form::submit('Passwort zusenden', ['class' => 'btn btn-primary']) !!}
		</div>

	{!! Form::close() !!}

@stop