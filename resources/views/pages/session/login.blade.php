@extends('layouts.master')

@section('content')

	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<h1>Login</h1>
			<div class="panel stacked">

				@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
				
				{!! Form::open(['route' => 'session.store']) !!}
					
					<div class="form-group">
						{!! Form::label('login', 'Benutzername oder Email') !!}
						{!! Form::text('login', null, ['class' => 'form-control']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('password', 'Password') !!}
						{!! Form::password('password', ['class' => 'form-control']) !!}
					</div>


					<div class="checkbox">
						<label>
							{!! Form::checkbox('remember', 'yes', true) !!}
							Eingeloggt bleiben
						</label>
						<span class="pull-right">
							{!! link_to_route('password.remind.show', 'Passwort vergessen') !!}
						</span>
					</div>	

					{!! Form::submit('Einloggen', ['class' => 'btn btn-blue center-block']) !!}

				{!! Form::close() !!}

			</div>
		</div>
	</div>


@stop