{!! Form::model($user->settings, ['route' => ['user.settings.update', $user->username], 'method' => 'put']) !!}
	
	<h3>Privatsphäre</h3>
	<hr>
	<div class="checkbox">
		<label>
			{!! Form::checkbox('showPostsInProfile') !!}
			Posts im eigenen Profil für andere Nutzer anzeigen
		</label>
	</div>

	<h3>E-Mail-Einstellungen</h3>
	<hr>
	<div class="checkbox">
		<label>
			{!! Form::checkbox('sendDailyMail') !!}
			Jeden Tag neue Posts zu den eigenen Interessen senden
		</label>
	</div>

	<h3>Flirtfunktion</h3>
	<hr>
    <div class="checkbox">
        <label>
            {!! Form::checkbox('wantsToFlirt') !!}
            Ich will meinen Traumpartner finden!
        </label>
    </div>

    <div class="form-group">
    	{!! Form::label('genderInterest', 'Ich suche...') !!}
    	{!! Form::select('genderInterest', ['male' => 'Männer', 'female' => 'Frauen'], null, ['class' => 'form-control']) !!}
    </div>

	{!! Form::submit('Speichern', ['class' => 'btn btn-blue']) !!}

{!! Form::close() !!}

<h3>Profil löschen</h3>
<hr>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi suscipit quo iure incidunt, culpa aspernatur sequi quidem harum ipsam sapiente voluptas quos animi iusto aliquam, ea voluptatum temporibus quam eum.</p>

{!! link_to_route('profile.confirm.delete', 'Profil löschen', [$user->username], ['class' => 'btn btn-red']) !!}
