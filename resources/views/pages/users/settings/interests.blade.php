{!! Form::model($user->interests, ['route' => ['profile.update.interests', $user->username], 'method' => 'put', 'files' => true]) !!}
					
	@foreach($tags as $tag)
		
		@if( $tag->children )
			<h3>{{ $tag->name }}</h3>
			<hr>
			<ul class="list-inline">
				@foreach($tag->children as $subtag)
					<li>
						<label for="{{$subtag->id}}">
							{!! Form::checkbox('interests[]', $subtag->id, $user->interests->contains($subtag->id), ['id' => $subtag->id]) !!}
							{{ $subtag->name }}
						</label>
					</li>
				@endforeach
			</ul>
		@endif

	@endforeach

	<div class="form-group">
		{!! Form::submit('Speichern', ['class' => 'btn btn-blue']) !!}
	</div>

{!! Form::close() !!}