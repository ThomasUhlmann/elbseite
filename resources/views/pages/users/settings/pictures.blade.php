<h3>Bilder</h3>
<hr>
{!! Form::model($user, ['route' => ['profile.update.avatar', $user->username], 'method' => 'put', 'files' => true]) !!}
					
	<div class="form-group">
		{!! Form::label('avatar', 'Profilbild') !!}
		{!! Form::file('avatar')!!}
	</div>

	<div class="form-group">
		{!! Form::submit('Speichern', ['class' => 'btn btn-blue']) !!}
	</div>

{!! Form::close() !!}