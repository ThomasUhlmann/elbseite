<?php use Elbsingles\Users\Profiles\ProfileOptions; ?>
{!! Form::model($user->profile, ['route' => ['profile.update', $user->username], 'method' => 'put']) !!}
	
	<div class="form-group">
		{!! Form::label('dob', 'Geburstdatum') !!}
		<div class="dropdown" dropdown>
			<ul class="dropdown-menu">
				<datetimepicker
					ng-init="data.date='{{ $user->dob->toAtomString() }}'"
					data-ng-model="data.date"
					data-on-set-time="console.log('test')"
					data-datetimepicker-config="{
						startView: 'year',
						minView: 'day'
					}"></datetimepicker>
			</ul>
			<input dropdown-toggle id="dob" type="text" value="@{{ data.date | date: 'dd.MM.yyyy' }}" class="form-control">
			<input type="hidden" name="dob" value="@{{ data.date | date: 'yyyy-MM-dd' }}">
		</div>
	</div>
	

	<div class="form-group">
		{!! Form::label('location', 'Stadt') !!}
		{!! Form::text('location', $user->location, ['class' => 'form-control']) !!}
	</div>	

	<div class="form-group">
		{!! Form::label('height', 'Größe') !!}
		{!! Form::select('height', ProfileOptions::height(), null, ['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('build', 'Figur') !!}
		{!! Form::select('build', ProfileOptions::$build, null, ['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('smoker', 'Raucher') !!}
		{!! Form::select('smoker', ProfileOptions::$smoker, null, ['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('vegetarian', 'Ernährung') !!}
		{!! Form::select('vegetarian', ProfileOptions::$vegetarian, null, ['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('children', 'Kinder') !!}
		{!! Form::select('children', ProfileOptions::$children, null, ['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('education', 'Bildung') !!}
		{!! Form::select('education', ProfileOptions::$education, null, ['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('about', 'Über mich') !!}
		{!! Form::textarea('about', null,['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::submit('Speichern', ['class' => 'btn btn-blue']) !!}
	</div>

{!! Form::close()!!}