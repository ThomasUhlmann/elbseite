@extends('layouts.master')

@section('content')

	<div class="row" ng-controller="SettingsController">
		<div class="col-md-3">
			<ul class="nav nav-pills nav-stacked">
				<li ng-class="{active: isSelected(1)}">
					<a href ng-click="selectTab(1)">Profil</a>
				</li>
				<li ng-class="{active: isSelected(2)}">
					<a href ng-click="selectTab(2)">Bilder</a>
				</li>
				<li ng-class="{active: isSelected(3)}">
					<a href ng-click="selectTab(3)">Interessen</a>
				</li>
				<li ng-class="{active: isSelected(4)}">
					<a href ng-click="selectTab(4)">Einstellungen</a>
				</li>
			</ul>
		</div>
		<div class="col-md-8 col-md-offset-1 main">
			<div ng-show="isSelected(1)">
				@include('pages.users.settings.profile')
			</div>
			<div ng-show="isSelected(2)">
				@include('pages.users.settings.pictures')
			</div>
			<div ng-show="isSelected(3)">
				@include('pages.users.settings.interests')
			</div>
			<div ng-show="isSelected(4)">
				@include('pages.users.settings.setup')
			</div>
		</div>
	</div>
	
@stop