@extends('layouts.master')

@section('content')

	<h1>Profil löschen</h1>

	<p>Bist du dir sicher, dass du dein Profil löschen möchtest?</p>
	<p>Du verlierst damit:</p>
	<ul>
		<li>All deine Nachrichten</li>
		<li>Deine Posts werden gelöscht</li>
		<li>Deine Kommentare werden gelöscht</li>
	</ul>

	{!! Form::open(['route' => ['profile.delete', $user->username], 'method' => 'delete']) !!}
		
		<p>Um deinen Account zu löschen, musst du hier aus Sicherheitsgründen dein Passwort eingeben.</p>

		<div class="form-group">
			{!! Form::label('password', 'Passwort') !!}
			{!! Form::password('password', ['class' => 'form-control']) !!}
		</div>

		{!! Form::submit('Profil löschen', ['class' => 'btn btn-red']) !!}
	{!! Form::close() !!}

@stop