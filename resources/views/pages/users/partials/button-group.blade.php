<div class="center-block" role="group">
	<a class="btn btn-default" href="{{ route('conversation.show', ['username' => $user->username]) }}" tooltip="Anschreiben">
		<div class="icon-mail"></div>
	</a>
	<button favorite-button class="btn btn-default" type="user" item="{{ $user->username }}" favorited="{{ $user->favoritedBy->contains(Auth::user()) }}" tooltip="Favorisieren"></button>
	<button block-button class="btn btn-default" username="{{ $user->username }}" blocked="{{ $user->isBlockedBy(Auth::user()) }}" tooltip="Blockieren"></button>
</div>

