<h3>Interessen</h3>
<div class="clearfix"></div>
<ul class="list-tags hover">
	@foreach($user->interests->sortBy('slug') as $interest)
		<li>{!! $interest->name !!}</li>
	@endforeach
</ul>