@if($user->isAuthUser())
	<a href="{{ route('profile.edit', ['username' => $user->username]) }}" class="edit-profile-link">
		<span class="icon-pencil"></span> Profil bearbeiten
	</a>
	<div class="clearfix"></div>
@endif