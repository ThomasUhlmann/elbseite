<div class="text-center">
	{{ $user->age }} Jahre 
	@if($user->location)
		<span class="icon-location"></span>
		{{ $user->location }} ({{ $user->location->distance(Auth::user()->location) }}km)
	@endif
</div>