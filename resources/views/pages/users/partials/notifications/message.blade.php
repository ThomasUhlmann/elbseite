{!! Form::open(['route' => ['notification.delete', $notification->id], 'method' => 'delete']) !!}
	{!! Form::submit('Nachricht löschen', ['class' => 'btn btn-sm btn-warning'])!!}
{!! Form::close() !!}
