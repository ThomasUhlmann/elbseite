{!! Form::open(['route' => 'friendship.confirm']) !!}
	{!! Form::hidden('user_id', $notification->notifiable->requester_id) !!}
	{!! Form::submit('Freundschaft bestätigen', ['class' => 'btn btn-success btn-sm']) !!}
{!! Form::close() !!}
{!! Form::open(['route' => ['friendship.reject', $notification->notifiable->requester_id]]) !!}
	{!! Form::hidden('user_id', $notification->notifiable->requester_id) !!}
	{!! Form::submit('Ablehnen', ['class' => 'btn btn-danger btn-sm']) !!}
{!! Form::close() !!}