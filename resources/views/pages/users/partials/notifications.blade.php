@if($user->isAuthUser())
	<ul>
		@foreach($user->notifications as $notification)
			<li>
				<h5>{{ $notification->subject }}</h5>
				<p>{{ $notification->body }}</p>
				@include($notification->present()->include_path)
			</li>
		@endforeach
	</ul>
@endif