@if($user->profile->hasInfos())
	<h3>Infos</h3>
	<div class="clearfix"></div>
	<dl class="user-infos">
		@if($user->profile->height)
			<dt>Größe</dt>
			<dd>{{ $user->present()->height }}</dd>
		@endif
		@if($user->profile->build)
			<dt>Figur</dt>
			<dd>{{ $user->present()->attribute('build') }}</dd>
		@endif
		@if($user->profile->smoker)
			<dt>Raucher</dt>
			<dd>{{ $user->present()->attribute('smoker') }}</dd>
		@endif
		@if($user->profile->children)
			<dt>Kinder</dt>
			<dd>{{ $user->present()->attribute('children') }}</dd>
		@endif
		@if($user->profile->education)
			<dt>Bildung</dt>
			<dd>{{ $user->present()->attribute('education') }}</dd>
		@endif
		@if($user->profile->vegetarian)
			<dt>Ernährung</dt>
			<dd>{{ $user->present()->attribute('vegetarian') }}</dd>
		@endif
	</dl>
@endif