@extends('layouts.master')

@section('content')

	<div id="user-profile">
		<div class="row">

			<!-- Show sidebar -->
			<div class="col-md-3" id="sidebar">
				{!! $user->present()->avatarImg !!}

				<h2 class="text-center">{{ $user->username }}</h2>

				@include('pages.users.partials.profileEditLink')
				
				@include('pages.users.partials.genderAge')
				
				<!-- @include('pages.users.partials.button-group') -->

				@include('pages.users.partials.interests')

				@include('pages.users.partials.infos')

			</div>


			<!-- Show stream -->
			<div class="col-md-8 col-md-offset-1 main">
				@include('pages.users.partials.about')
				@if($user->isAuthUser() || $user->settings->showPostsInProfile)
					<section id="stream" ng-controller="StreamListController" ng-cloak>
						@include('layouts.stream.list')
					</section>
				@endif
			</div>
		</div>
	</div>

@stop