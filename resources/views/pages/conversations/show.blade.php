@extends('layouts.master')

@section('content')

	<div ng-controller="MessagesController" ng-init="username = '{{ $user->username }}'">
		
		<ul class="conversations panel stacked" >

			<li class="head">
				<div style="display: inline-block;">
					<a href="{{ URL::previous() == URL::current() ? URL::route('conversation.inbox') : URL::previous() }}">
						<span class="icon-level-up"></span> zurück
					</a>
				</div>
				<div class="pull-right">
					{!! Form::open(['route' => ['conversation.doArchive', $user->username]]) !!}
						<button type="submit" class="">
							<span class="icon-download"></span>
							Archivieren
						</button>
					{!! Form::close() !!}
					<form ng-submit="deleteConversation('{{ $user->username }}')">
						<button type="submit">
							<span class="icon-trash"></span>
							Löschen
						</button>
					</form>
				</div>
			</li>

			<li class="message">
				
				<form ng-submit="postNewMessage()">
					<div class="form-group">
						<label for="body"></label>
						<textarea msd-elastic ng-model="newMessage.body" id="body" class="form-control" rows="4" required></textarea>
					</div>

					<button ng-click="" class="btn btn-blue">Nachricht senden</button>
				</form>

			</li>

			<div infinite-scroll="moreMessages()" infinite-scroll-disabled="busy" infinite-scroll-distance="2" ng-cloak>
				<li class="message" ng-repeat="message in messages">
					<div class="time">
						@{{ message.sendAt | amCalendar }}
					</div>
					<avatar context="'message'"></avatar>
					<div class="wrapper">
						<span class="username">@{{ message.user.username }}</span>
						<div ng-bind-html="message.body | linkyUnsanitized:'_blank' | nl2br"></div>
					</div>
					<div class="clearfix"></div>
				</li>
			</div>
		</ul>
		
		<div class="ajax-loader" ng-show="busy"></div>
	</div>


@stop