@extends('layouts.master')

@section('content')
	<h1>Nachrichten schreiben</h1>
	
	{!! Form::open(['route' => 'message.store']) !!}
		
		{!! Form::hidden('to_user', Input::get('user_id')) !!}

		<div class="form-group">
			{!! Form::label('body', 'Nachricht') !!}
			{!! Form::text('body', null, ['class' => 'form-control']) !!}
		</div>

		{!! Form::submit('Abschicken', ['class' => 'btn btn-primary']) !!}

	{!! Form::close() !!}

@stop