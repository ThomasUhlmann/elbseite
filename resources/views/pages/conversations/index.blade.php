@extends('layouts.master')

@section('content')
	<ul class="conversations">
		<li class="head">
			<div style="display: inline-block;">
				<a href="{{ URL::route('conversation.inbox') }}">
					<span class="icon-mail"></span>
					Posteingang 
					({{ Auth::user()->conversations()->inbox()->unread()->count() == 0 ? '' : Auth::user()->conversations()->inbox()->unread()->count() }})
				</a>
				<a href="{{ URL::route('conversation.outbox') }}">
					<span class="icon-paperplane"></span>
					Gesendet
				</a>
				<a href="{{ URL::route('conversation.archive') }}">
					<span class="icon-stack"></span>
					Archiv
				</a>
			</div>
			<div class="pull-right">
				@if( ! $conversations->isEmpty())
					{{ $conversations->firstItem() }}-{{ $conversations->lastItem() }} von {{ $conversations->total() }}
				@endif
			</div>
		</li>
		@forelse($conversations as $conversation)
			<li class="conversation">
				<a href="/nachrichten/{{ $conversation->talkingTo()->username }}">
					<div class="time">
						{{ $conversation->updated_at->format('d.m.Y H:i') }}
					</div>
					<div class="avatar pull-left">
						{!! $conversation->talkingTo()->present()->thumbnail !!}
					</div>
					<div class="wrapper pull-left">
						<span class="username">
							{{ $conversation->talkingTo()->username }}
							@if( ! $conversation->isRead)
								<span class="label label-warning">Ungelesen</span>
							@endif
						</span>
						<span class="preview @if( ! $conversation->isRead) bold @endif">
							{!! $conversation->preview() !!}...
						</span>
					</div>
				</a>
				<div class="clearfix"></div>
			</li>
		@empty
			<li class="conversation">Keine Nachrichten</li>
		@endforelse
		<li class="footer">
			<div class="pull-right">
				{!! $conversations->render() !!}
			</div>
			<div class="clearfix"></div>
		</li>
	</ul>

@stop