<?php
    $presenter = new Elbsingles\Messages\MessagesPagination($paginator);
?>

<?php if ($paginator->getLastPage() > 1): ?>
    <ul class="conv-pag">
            <?php echo $presenter->render(); ?>
    </ul>
<?php endif; ?>
