@extends('layouts.master')

@section('content')

	@foreach($errors->all() as $error)
		<li>{!! $error !!}</li>
	@endforeach

	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<h1>Mitmachen</h1>
			<div class="panel stacked">
				{!! Form::open(['route' => 'register.store']) !!}
					
					<div class="form-group">
						{!! Form::label('username', 'Nutzername') !!}
						{!! Form::text('username', null, ['class' => 'form-control', 'required' => 'required']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('email', 'Email') !!}
						{!! Form::text('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('password', 'Passwort') !!}
						{!! Form::password('password', ['class' => 'form-control', 'required' => 'required']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('password_confirmation', 'Passwort wiederholen') !!}
						{!! Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required']) !!}
					</div>

					{!! Form::submit('Registrieren', ['class' => 'btn btn-blue']) !!}

				{!! Form::close() !!}
			</div>

		</div>
	</div>





@stop