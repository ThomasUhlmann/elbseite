@extends('layouts.master')

@section('content')

	<tabset justified="true" ng-cloak>
		<tab heading="Nutzer blockieren">
			<div class="panel stacked">
				@include('pages.help.partials.blocking')
			</div>
		</tab>
		<tab heading="Profil">
			<div class="panel stacked">
				Short Labeled Justified content
			</div>
		</tab>
		<tab heading="Messages">
			<div class="panel stacked">
				Long Labeled Justified content
			</div>
		</tab>
	</tabset>

@stop