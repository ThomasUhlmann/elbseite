<p>
	Möchtest Du andere Nutzer blockieren, kannst du das ganz einfach über folgendes Symbol tun. [Bild einfügen]
	Hast du einen Nutzer blockiert, dann
</p>
<ul>
	<li>siehst du keine Meldungen mehr vom diesem Nutzer</li>
	<li>der betreffende Nutzer sieht keine Meldungen mehr von dir</li>
	<li>der Nutzer kann dein Profil nicht mehr besuchen</li>
	<li>der Nutzer kann dir keine Nachrichten mehr schicken</li>
	<li>du kannst dem Nutzer keine Nachrichten mehr schicken</li>
</ul>
<p>
	Einzig bei den Kommentaren von Meldung anderer Nutzer wirst du den Nutzer noch sehen und der andere Nutzer kann dich auch noch sehen.
</p>