@extends('layouts.master')

@section('content')

	<section id="singles-list">

		<h1>Andere Nutzer</h1>

		<div class="row">

			@forelse($favoritedUsers as $user)

				<div class="col-md-3">
					<article class="panel stacked">
						<div class="user">
							{!! $user->present()->thumbnail !!}
							<div class="wrapper">
								{!! $user->present()->profileLink !!}
								<span>Alter: {{ $user->age }}</span>
							</div>
						</div>
					</article>

				</div>

			@empty

				<p>Niemand gefunden!</p>

			@endforelse

		</div>
		
	</section>

	<section id="singles-list">

		<h1>Lieblingsposts</h1>

		<div class="row">

			@forelse($favoritedPosts as $post)

				<div class="col-md-3">

					<article class="panel stacked">
						<div class="user">
							{!! $post->title !!}
							<div class="wrapper">
								{!! $post->body !!}
							</div>
						</div>
					</article>

				</div>

			@empty

				<p>Niemand gefunden!</p>

			@endforelse

		</div>
		
	</section>

	<section>
		<h3>Hat Geblockt</h3>
		@foreach(Auth::user()->hasBlocked as $blocked)
			<a href="{{ URL::route('profile', [$blocked->username])}}">
				{{ $blocked->username }}
			</a>
		@endforeach
		<h3>Geblockt von</h3>
		@foreach(Auth::user()->blockedBy as $beenBlocked)
			{{ $beenBlocked->username }}
		@endforeach
	</section>

@stop