@extends('layouts.master')

@section('content')

	<div id="events-calendar">

			<div class="row">
				<div class="col-md-3" id="sidebar">
					@include('widgets.missingInfo')				

					@if(Auth::user()->wantsToFlirt())
						@include('widgets.newSingles')
					@endif

					@include('widgets.myActivities')
				</div>
				<div class="col-md-8 col-md-offset-1 main">
					
					<section id="stream" ng-controller="StreamListController" ng-cloak>
						@include('layouts.stream.list')
					</section>

				</div>
			</div>
	</div>

@stop