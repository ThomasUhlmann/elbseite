@extends('layouts.master')

@section('content')

	<div class="main">
		<div ng-controller="SinglesSearchController">

			<div id="singles-search">
				<form class="form-inline">

					Showing you {{ Auth::user()->settings->genderInterest }}

					<div class="form-group">
						<label for="begin">Alter von</label>
						<input type="text" ng-model="ageSpan.begin">
					</div>

					<div class="form-group">
						<label for="end">Alter bis</label>
						<input type="text" ng-model="ageSpan.end">
					</div>

				</form>
			</div>
		</div>

		<div ng-controller="SinglesListController" ng-cloak>

			<section id="singles-list">

				<div class="row">

					<div class="col-md-3 col-sm-6"
						 ng-repeat="single in filteredSingles = ( singles | ageBetween : ageSpan.begin : ageSpan.end)"
						 ng-class="{clearfix: ($index + 1) % 3 == 0}">
						<article class="single">
							<avatar context="'single'"></avatar>
							<div class="basic-infos wrapper">
								<h1>
									<a href="/@@{{ single.username }}">@{{ single.username }}</a>
								</h1>
								@{{ single.age }} Jahre
								<span class="icon-location"></span> @{{ single.location.city }}
								<span ng-if="single.location.distance >= 5">
									(@{{ single.location.distance }}km)
								</span>
								<div class="clearfix"></div>
								<span class="label label-default" ng-class="single.last_active.css">
									@{{ single.last_active.text }}
								</span>
							</div>
							<div class="details">
								<!-- <h4>Interessen</h4>
								<ul class="list-tags">
									<li ng-repeat="interest in single.interests">@{{ interest.name }}</li> 
								</ul> -->
								<!-- <h4>Infos</h4>
								<ul class="list-unstyled">
									<li ng-if="single.profile.height">Größe: @{{ single.profile.height }}</li>
									<li ng-if="single.profile.build">Figur: @{{ single.profile.build }}</li>
									<li ng-if="single.profile.smoker">Raucher: @{{ single.profile.smoker}}</li>
									<li ng-if="single.profile.children">Kinder: @{{ single.profile.children }}</li>
									<li ng-if="single.profile.education">Bildung: @{{ single.profile.education }}</li>
									<li ng-if="single.profile.vegetarian">Ernährung: @{{ single.profile.vegetarian }}</li>
								</ul> -->
							</div>
						</article>
						<div ></div>	
					</div>

					<div ng-show="filteredSingles.length === 0 && ageSpan.begin">Keine Singles gefunden</div>

				</div>
				
			</section>
			
		</div>
	</div>
			
@stop