@extends('layouts.master')

@section('content')

	<div class="row">
		<div class="col-md-3" id="sidebar">
			@include('widgets.missingInfo')				

			@if(Auth::user()->wantsToFlirt())
				@include('widgets.newSingles')
			@endif

			@include('widgets.myActivities')
			@include('widgets.upcomingEvents')
		</div>
		<div class="col-md-8 col-md-offset-1 main">
				@include('layouts.stream.controller')
			<section id="stream" ng-controller="StreamListController" ng-cloak>
				@include('layouts.stream.postForm')
				<hr>
				@include('layouts.stream.list')
			</section>
		</div>
	</div>


@stop