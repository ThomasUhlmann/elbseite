@extends('layouts.master')

@section('header')

    <div class="top-area">
        <div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-6">
					<div class="panel panel-red">
						<div class="panel-body">
							<h1>Neue Leute aus Dresden und Umgebung kennenlernen</h1>
							<p class="lead">
								Hier hast Du die Möglichkeit, Menschen zu treffen, die Deine Interessen teilen, Freundschaften zu knüpfen und Dich vielleicht sogar neu zu verlieben.
							</p>
							<p class="lead">
								Die Elbseite ist eine Plattform von Dresdnern für Dresdner. Für immer kostenlos. Sei auch Du dabei.
							</p>
							<a href="{{ URL::route('register') }}" class="btn btn-sign-up">
								Hier anmelden!
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop

@section('content')

	<div class="row">
		<div class="col-md-6">
			<img src="http://placehold.it/500x250" class="bordered">
		</div>
		<div class="col-md-6">
			<h2>Wähle Deine Interessen aus.</h2>
			<p>
				Egal ob Du Sportfanatiker, Theatergänger oder Partyluder bist - auf unserer Plattform findest Du Leute, die Deine Interessen teilen und wir machen es Dir einfach, diese Menschen zu finden. So kannst Du einfach Deinen nächsten Spieleabend, eine Wanderung oder einen Kinoabend organisieren. Lass Deiner Kreativität freien Lauf und finde Mitmacher auf unserer Plattform.
			</p>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<h3>
				Für Unternehmungslustige
			</h3>
			<ul>
				<li>Sie Dir an, was andere planen und nimm daran teil oder poste selbst, was Dich interessiert.</li>
				<li>Filtere je nach dem, wonach Dir gerade ist.</li>
				<li>Suche im Veranstaltungskalender nach Unternehmungen</li>
			</ul>
		</div>
		<div class="col-md-6">
			<h3>
				Für Herzensbrecher
			</h3>
			<ul>
				<li>Suche nach Singles, die ebenfalls Interesse an einer Partnerschaft haben.</li>
				<li>Schreibe Nachrichten an deine Favoriten</li>
				<li>Dresdens einzige kostenlose Partnerbörse</li>
			</ul>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<h2>Weitere Features</h2>
			<ul>
				<li>Blockiere unerwünschte Nutzer</li>
				<li>Passe Deine Privatsphäre an</li>
				<li>Finde Leute aus Dresden uns Umgebung</li>
			</ul>
		</div>
	</div>

@stop