<?php

return [

	/* Page name */
	'page_name' => 'Elbseite',

	/* Path to the default avatar */
	'default_avatar' => [
		'male'   => '/img/avatars/default-male.png',
		'female' => '/img/avatars/default-female.png',
	],

	/* Time span in minutes for updating the last_active column of the current user */
	'minutes_between_update' => 15,

	/*=======================================
	=            STREAM Settings            =
	=======================================*/
	
	'stream' => [
		'paginate' => 10,
	],
	
	/*========================================
	=            MAILBOX Settings            =
	========================================*/

	'mailbox' => [
		'conversations' => [
			'paginate' => 8,
		],
		'messages' => [
			'paginate' => 8,
		],
	],

];