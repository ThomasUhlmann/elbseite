(function() {

	var app = angular.module('elbseite', [
		'infinite-scroll',
		'smoothScroll',
		'checklist-model',
		'ngSanitize',
		'ui.bootstrap',
		'ui.bootstrap.datetimepicker',
		'angularMoment',
		'monospaced.elastic',
	]);

	app.config(['$httpProvider', function ($httpProvider) {
		$httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
	}]);
	
	app.run(['amMoment', function(amMoment) {
			amMoment.changeLocale('de');
		}]);
}());

(function() {

  var ageBetween = function () {
    return function (singles, arg1, arg2) {

      var ageBegin = arg1 || 18,
          ageEnd = arg2 || 120,
          filtered = [];
      
      for (var i = 0; i < singles.length; i++) {
        var single = singles[i];

        if (single.age >= ageBegin && single.age <= ageEnd) {
          filtered.push(single);
        }
      }
      return filtered;
    };
  }

  angular.module('elbseite').filter('ageBetween', ageBetween);

}());

(function() {

   var linkyUnsanitized = ['$sanitize', function($sanitize) {
    var LINKY_URL_REGEXP = 
          /((ftp|https?):\/\/|(www\.)|(mailto:)?[A-Za-z0-9._%+-]+@)\S*[^\s.;,(){}<>"”’]/,
        MAILTO_REGEXP = /^mailto:/;

    return function(text, target) {
      if (!text) return text;
      var match;
      var raw = text;
      var html = [];
      var url;
      var i;
      while ((match = raw.match(LINKY_URL_REGEXP))) {
        // We can not end in these as they are sometimes found at the end of the sentence
        url = match[0];
        // if we did not match ftp/http/mailto then assume mailto
        if (match[2] == match[3]) url = 'mailto:' + url;
        i = match.index;
        addText(raw.substr(0, i));
        addLink(url, match[0].replace(MAILTO_REGEXP, ''));
        raw = raw.substring(i + match[0].length);
      }
      addText(raw);
      return html.join('');

      function addText(text) {
        if (!text) {
          return;
        }
        html.push(text);
      }

      function addLink(url, text) {
        html.push('<a ');
        if (angular.isDefined(target)) {
          html.push('target="');
          html.push(target);
          html.push('" ');
        }
        html.push('href="');
        html.push(url);
        html.push('">');
        addText(text);
        html.push('</a>');
      }
    };
  }];

  angular.module('elbseite').filter('linkyUnsanitized', linkyUnsanitized);

}());
(function() {

	var nl2br = function() {
		return function(text) {
			return text.replace(/\n/g, '<br/>');
		}
  	};

	angular.module('elbseite').filter('nl2br', nl2br);
}());
angular.module('elbseite').service('modalService', ['$modal',
    function ($modal) {

        var modalDefaults = {
            backdrop: true,
            keyboard: true,
            modalFade: true,
            templateUrl: '/templates/shared/modal.html'
        };

        var modalOptions = {
            closeButtonText: 'Close',
            actionButtonText: 'OK',
            actionButtonShow: true,
            headerText: 'Proceed?',
        };

        this.showModal = function (customModalDefaults, customModalOptions) {
            if (!customModalDefaults) customModalDefaults = {};
            return this.show(customModalDefaults, customModalOptions);
        };

        this.show = function (customModalDefaults, customModalOptions) {
            //Create temp objects to work with since we're in a singleton service
            var tempModalDefaults = {};
            var tempModalOptions = {};

            //Map angular-ui modal custom defaults to modal defaults defined in service
            angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

            //Map modal.html $scope custom properties to defaults defined in service
            angular.extend(tempModalOptions, modalOptions, customModalOptions);

            if (!tempModalDefaults.controller) {
                tempModalDefaults.controller = function ($scope, $modalInstance) {
                    $scope.modalOptions = tempModalOptions;
                    $scope.modalOptions.ok = function (result) {
                        $modalInstance.close(result);
                    };
                    $scope.modalOptions.close = function (result) {
                        $modalInstance.dismiss('cancel');
                    };
                }
            }

            return $modal.open(tempModalDefaults).result;
        };

    }]);
(function () {

	var loginButton = function() {

		var loginButtonController = ['$scope', '$http', '$attrs', 'modalService', function($scope, $http, $attrs, modalService) {
		
					$scope.open = function() {
		
						var modalOptions = {
								closeButtonText: 'Abbrechen',
								actionButtonShow: false,
								actionButtonText: 'Hey',
								template: 'templates/login/form.html'
							};
		
							modalService.showModal({size: 'sm'}, modalOptions)
					}
				}];

		return {
				controller: loginButtonController,
				scope: true,
			};
	}

	angular.module('elbseite').directive('loginButton', loginButton);

}());
(function() {

	var loginController = ['$scope', '$http', '$window', function($scope, $http, $window) {
	
			$scope.inputData = {
				remember: true
			};
			$scope.message = '';
	
			$scope.login = function() {
	
				if ($scope.message) {
					$scope.message = 'Login wird durchgeführt.'
				};
	
				$http.post('/login', $scope.inputData)
					.success(function(data) {
						if (data.login == true)
						{
							$window.location.replace('/');
						}
						else
						{
							$scope.message = 'Die Eingabe war nicht richtig.';
							$scope.inputData.password = '';
						}
					});
			}
	
		}];

	angular.module('elbseite').controller('LoginController', loginController);

}());
(function() {

	var settingsController = ['$scope', function($scope) {

			var activeTab = 1;

			$scope.isSelected = function(tab) {
				return tab == activeTab;
			}

			$scope.selectTab = function(tab) {
				activeTab = tab;
			}
	
		}];

	angular.module('elbseite').controller('SettingsController', settingsController);

}());
(function() {

	var messagesController = ['$scope', '$http', '$window', 'modalService', function($scope, $http, $window, modalService) {
	
			$scope.messages = [];
			$scope.busy = true;
			var nextPageUrl = '';
	
			$scope.$watch('username', function(username) {
				$http.get('/api/messages/' + username)
					.success(function(data) {
						$scope.messages = data.messages;
						nextPageUrl = data.nextPageUrl;
						$scope.busy = false;
					});
			});
	
			$scope.moreMessages = function() {
	
				if($scope.busy || nextPageUrl == null) return; 
				$scope.busy = true;
	
				$http.get(nextPageUrl)
					.success(function(data) {
						for (var i = 0; i < data.messages.length; i++) {
							$scope.messages.push(data.messages[i]);
						};
						nextPageUrl = data.nextPageUrl;
						$scope.busy = false;
					});
			}
	
			$scope.postNewMessage = function() {
	
				$http.post('/api/messages/' + $scope.username + '/store', $scope.newMessage)
					.success(function(data) {
						$scope.messages.unshift(data.newMessage[0]);
						$scope.newMessage = {};
					});
			}
	
			$scope.deleteConversation = function(username) {
	
				var modalOptions = {
					closeButtonText: 'Abbrechen',
					actionButtonText: 'Nachrichten löschen',
					bodyText: 'Diese Nachrichten wirklich löschen?'
				};
	
				modalService.showModal({size: 'sm'}, modalOptions)
							.then(function (result) {
								$http.post('/api/messages/' + username + '/delete')
									.success(function() {
										$window.location.replace('/nachrichten');
									});
							});
			}
		}];

	angular.module('elbseite').controller('MessagesController', messagesController);

}());
(function() {

	var singlesListController = ['$scope', '$http', 'singlesService', function($scope, $http, singlesService) {
	
			$scope.singles = {};
			$scope.ageSpan = {};
	
			$scope.$watchGroup([
				function() { return singlesService.singles(); },
				function() { return singlesService.ageSpan(); },
			], function() {
				$scope.singles = singlesService.singles();
				$scope.ageSpan = singlesService.ageSpan();
			});
		}];

	angular.module('elbseite').controller('SinglesListController', singlesListController);

}());
(function() {

	var singlesSearchController = ['$scope', '$http', 'singlesService', function($scope, $http, singlesService) {
	
			$scope.genders = [
				{label: 'Männer', value: 'male'},
				{label: 'Frauen', value: 'female'}
			];
	
			$scope.$watch( function() { return singlesService.ageSpan(); }, function() {
				$scope.ageSpan = singlesService.ageSpan();
			});
		}];

	angular.module('elbseite').controller('SinglesSearchController', singlesSearchController);

}());
(function() {

	var singlesService = ['$http', function($http) {
	
			var singles = {},
				ageSpan = {};
	
			$http.get('/api/singles/').success(function(data) {
				singles = data.singles;
				ageSpan = {
					begin: (data.user.age - 8) >= 18 ? data.user.age - 8 : 18,
					end: data.user.age + 8
				};
			});
	
			return {
				singles: function() { return singles; },
				ageSpan: function() { return ageSpan },
			}
		}];

	angular.module('elbseite').factory('singlesService', singlesService);

}());
(function() {

	var streamFilterController = ['$scope', 'filterFilter', 'streamService', function($scope, filterFilter, streamService) {
	
			$scope.interests = streamService.interests();
			$scope.showInterests = false;
			$scope.selected = [];
	
			$scope.$watch( function() { return streamService.interests(); }, function () {
				$scope.interests = streamService.interests();
				$scope.selected = filterFilter($scope.interests, {selected: true}).map(function(interest) { return interest.id });
			});
	
			$scope.update = function() {
	
				streamService.setUrl(null);
				streamService.fetchPosts({
						tags: $scope.selected
					}, true);
			};
	
			$scope.toggleInterests = function() {
	
				$scope.showInterests = ! $scope.showInterests;
			};
	
			$scope.deselectAll = function() {
	
				streamService.deselectAll();
			};
	
			$scope.selectAll = function() {
	
				streamService.selectAll();
			};
		}];

	angular.module('elbseite').controller('StreamFilterController', streamFilterController);

}());
(function() {

	var streamListController = ['$scope', '$http', 'filterFilter', 'streamService', function($scope, $http, filterFilter, streamService) {
	
			$scope.interests = [];
			$scope.posts = {};
			$scope.busy = false;
			$scope.endReached = false;
	
			$scope.$watch( function() { return streamService.posts(); }, function() {
				$scope.posts = streamService.posts();
			});
	
			$scope.$watch( function() { return streamService.interests(); }, function() {
				$scope.interests = streamService.interests();
			});
	
			$scope.$watch( function() { return streamService.busy(); }, function() {
				$scope.busy = streamService.busy();
			});
	
			$scope.$watch( function() { return streamService.endReached(); }, function() {
				$scope.endReached = streamService.endReached();
			});
	
			$scope.fetchPosts = function(query) {
	
				streamService.fetchPosts(query);
			}
	
			$scope.submitPost = function() {
	
				streamService.storePost($scope.postData);
				$scope.postData = {};
			}
	
		}];

	angular.module('elbseite').controller('StreamListController', streamListController);

}());
(function() {

	var streamService = ['$http', 'authUser', function($http, authUser) {
	
			var posts = [],
				busy = false,
				endReached = false,
				nextPageUrl = null,
				interests = [];
	
				$http.get('/api/interests')
					.success(function(data) {
						interests = data;
					});
	
			return {
				posts: function() { return posts; },
				busy: function() { return busy; },
				endReached: function() { return endReached; },
				interests: function() { return interests; },
				nextPageUrl: function() { return nextPageUrl; },
	
				fetchPosts: function(query, reset) {
	
					if(busy || endReached) return; 
					if(reset) posts = [];
					url = nextPageUrl || '/api/stream/?' + $.param(query || '');
	
					busy = true;
	
					$http.get(url)
						.success(function(data) {
							for (var i = 0; i < data.posts.length; i++) {
								posts.push(data.posts[i]);
							};
							nextPageUrl = data.nextPageUrl;
							if(nextPageUrl == null) endReached = true;
							busy = false;
							console.log('New push');
							if (data.posts.length) {
								console.log('new posts');
							}
							else {
								console.log('no posts');
							}

						});
				},
	
				deletePost: function(index) {
	
					$http.delete('/api/post/' + posts[index].id + '/delete')
						.success(function() {
	
							posts.splice(index, 1);
						});
				},
	
				setUrl: function(url) {
					nextPageUrl = url;
					endReached = false;
				},
	
				storePost: function(postData) {
					
					$http.post('/api/post/store', postData)
						.success(function(data) {
							posts.unshift(data.newPost[0]);
						})
				},
	
				deselectAll: function() {
	
					interests = angular.copy(interests.map(function(interest) {
						interest.selected = false;
						return interest;
					}));
				},
	
				selectAll: function() {
	
					interests = angular.copy(interests.map(function(interest) {
						interest.selected = true
						return interest;
					}));
				}
			}
		}];

	angular.module('elbseite').factory('streamService', streamService);

}());
(function() {

	var commentsTemplate = function() {

		var commentsController = ['$scope', '$http', '$timeout', 'modalService', function($scope, $http, $timeout, modalService) {
		
					$scope.comments = $scope.post.comments;
					$scope.limit = -3;
					$scope.expandClass = 'icon-chevron-down-circle';
					$scope.showAll = false;
					$scope.isCreating = false;
					$scope.isActive = false;
		
					$scope.submitComment = function() {
		
						$http.post('/api/' + $scope.post.type + '/' + $scope.post.id + '/comments/store', $scope.commentData)
							.success(function(newComment) {
								$scope.comments.push(newComment);
								$scope.commentData = {};
								$scope.isCreating = false;
								$scope.isActive = false;
							})
					}
		
					$scope.deleteComment = function(index) {
		
						var modalOptions = {
								closeButtonText: 'Abbrechen',
								actionButtonText: 'Kommentar löschen',
								bodyText: 'Diesen Kommentar wirklich löschen?'
							};
		
							modalService.showModal({size: 'sm'}, modalOptions)
										.then(function (result) {
											$http.delete('/api/' + $scope.post.type + '/' + $scope.post.id + '/comments/' + $scope.comments[index].id + '/delete')
												.success(function() {
													$scope.comments.splice(index, 1);
												});
										});
		
					}
		
					$scope.toggleComments = function() {

						if($scope.comments.length <= 3) return;
		
						if ($scope.limit === 'Infinity')
						{
							$scope.limit = -3;
							$scope.showAll = false;
						}
						else
						{
							$scope.limit = 'Infinity';
							$scope.showAll = true;
						}
					}
		
					$scope.canExpandComments = function() {
		
						return $scope.comments.length >= 4;
					}
		
					$scope.startCreating = function() {
		
						$scope.isCreating = true;
						$scope.isActive = true;
						$timeout(function() {
							$('textarea.active').focus();
						});
					}
		
					$scope.unfocus = function() {
						$scope.isActive = false;
					}
		
					$scope.stopCreating = function() {
		
						$scope.isCreating = false;
					}
		
					$scope.$watch('showAll', function(newval, oldval) {
						if (newval == true)
						{
							$scope.expandClass = 'icon-chevron-up-circle';
						}
						else
						{
							$scope.expandClass = 'icon-chevron-down-circle';
						}
					});
				}];

		return {
			templateUrl: 'templates/posts/comments.html',
			controller: commentsController
		};
	}

	angular.module('elbseite').directive('comments', commentsTemplate);

}());
(function() {

	var postTemplate = function() {

		var postController = ['$scope', 'streamService', 'modalService', function($scope, streamService, modalService) {
				
						$scope.template = '/templates/posts/content/' + $scope.post.type + '.html'
		
						$scope.isLinked = function() {
		
							return $scope.post.type != 'post';
						}
		
						$scope.deletePost = function(index) {
		
							var modalOptions = {
								closeButtonText: 'Abbrechen',
								actionButtonText: 'Beitrag löschen',
								bodyText: 'Diesen Beitrag wirklich löschen?'
							};
		
							modalService.showModal({size: 'sm'}, modalOptions)
										.then(function (result) {
											streamService.deletePost(index);
										});
						}
					}];
		
		return {
			templateUrl: '/templates/posts/post.html',
			controller: postController
		};
	}

	angular.module('elbseite').directive('post', postTemplate);

}());
/**
 * Checklist-model
 * AngularJS directive for list of checkboxes
 */

angular.module('checklist-model', [])
.directive('checklistModel', ['$parse', '$compile', function($parse, $compile) {
  // contains
  function contains(arr, item) {
    if (angular.isArray(arr)) {
      for (var i = 0; i < arr.length; i++) {
        if (angular.equals(arr[i], item)) {
          return true;
        }
      }
    }
    return false;
  }

  // add
  function add(arr, item) {
    arr = angular.isArray(arr) ? arr : [];
    for (var i = 0; i < arr.length; i++) {
      if (angular.equals(arr[i], item)) {
        return arr;
      }
    }    
    arr.push(item);
    return arr;
  }  

  // remove
  function remove(arr, item) {
    if (angular.isArray(arr)) {
      for (var i = 0; i < arr.length; i++) {
        if (angular.equals(arr[i], item)) {
          arr.splice(i, 1);
          break;
        }
      }
    }
    return arr;
  }

  // http://stackoverflow.com/a/19228302/1458162
  function postLinkFn(scope, elem, attrs) {
    // compile with `ng-model` pointing to `checked`
    $compile(elem)(scope);

    // getter / setter for original model
    var getter = $parse(attrs.checklistModel);
    var setter = getter.assign;

    // value added to list
    var value = $parse(attrs.checklistValue)(scope.$parent);

    // watch UI checked change
    scope.$watch('checked', function(newValue, oldValue) {
      if (newValue === oldValue) { 
        return;
      } 
      var current = getter(scope.$parent);
      if (newValue === true) {
        setter(scope.$parent, add(current, value));
      } else {
        setter(scope.$parent, remove(current, value));
      }
    });

    // watch original model change
    scope.$parent.$watch(attrs.checklistModel, function(newArr, oldArr) {
      scope.checked = contains(newArr, value);
    }, true);
  }

  return {
    restrict: 'A',
    priority: 1000,
    terminal: true,
    scope: true,
    compile: function(tElement, tAttrs) {
      if (tElement[0].tagName !== 'INPUT' || !tElement.attr('type', 'checkbox')) {
        throw 'checklist-model should be applied to `input[type="checkbox"]`.';
      }

      if (!tAttrs.checklistValue) {
        throw 'You should provide `checklist-value`.';
      }

      // exclude recursion
      tElement.removeAttr('checklist-model');
      
      // local scope var storing individual checkbox model
      tElement.attr('ng-model', 'checked');

      return postLinkFn;
    }
  };
}]);
(function () {

	var favoriteButton = function() {

		var favoriteButtonController = ['$scope', '$http', '$attrs', function($scope, $http, $attrs) {
					
					$scope.buttonClass = getButtonClass($attrs.favorited);
		
					$scope.$watch($attrs.favorited, function (newval, oldval) {
		                if (newval) {
							$scope.buttonClass = getButtonClass(newval);
		                }
		            });
					
					$scope.rememberThis = function() {
		
						var url = '/api/favorites/' + $attrs.item + '/remember?type=' + $scope.post.type;
		
						$http.post(url)
							.success(function(data) {
								$scope.buttonClass = getButtonClass(data.favorited);
							});
					}
		
					function getButtonClass (favorited) {
						return favorited == true ? 'icon-star-two active' : 'icon-star';
					}
				}];

		return {
				templateUrl: 'templates/shared/favoriteButton.html',
				controller: favoriteButtonController,
			};
	}

	angular.module('elbseite').directive('favoriteButton', favoriteButton);

}());
(function () {

	var feedbackButton = function() {

		var feedbackModalController = ['$scope', '$http', '$modalInstance', function($scope, $http, $modalInstance) {
		
					$scope.feedback = {
						message: ''
					};
		
					$scope.send = function() {
		
						$http.post('/api/feedback/store', $scope.feedback)
								.success(function(reponse) {
									$modalInstance.dismiss('cancel');
								});
					}
				}];

		var feedbackButtonController = ['$scope', '$http', '$attrs', 'modalService', function($scope, $http, $attrs, modalService) {
		
					$scope.open = function() {
		
						var modalOptions = {
								closeButtonText: 'Abbrechen',
								actionButtonText: 'Feedback senden',
								actionButtonShow: true,
							};
		
						modalService.showModal({
							controller: feedbackModalController,
							templateUrl: 'templates/shared/feedbackBody.html',
						}, modalOptions)
									.then(function (result) {
		
									});
					}
				}];

		return {
				templateUrl: '/templates/shared/feedbackButton.html',
				controller: feedbackButtonController,
				scope: true,
				replace: true
			};
	}

	angular.module('elbseite').directive('feedbackButton', feedbackButton);

}());
(function () {

	var scrollToTop = ['$window', 'smoothScroll', function($window, smoothScroll) {

		var link = function(scope, element, attrs) {

			scope.visible = false;

			angular.element($window).bind('scroll', function() {
				if (this.pageYOffset >= 100) {
					scope.visible = true;
				} else {
					scope.visible = false;
				}
				scope.$apply();
			});

			scope.scroll = function() {
				var options = attrs;
				var body = document.getElementById('body');

				smoothScroll(body, options);
			}
		}

		return {
				templateUrl: '/templates/shared/scrollToTop.html',
				link: link,
				replace: true
			};
	}];

	angular.module('elbseite').directive('scrollToTop', scrollToTop);

}());
(function() {

	var avatarTemplate = ['authUser', function(authUser) {

				var link = function ($scope, $element, $attrs) {
					
					$scope.$watch($attrs.context, function(context) {
	
						if ((context !== null) && (context !== undefined) && (context !== '')) {
	
							var size = $attrs.size || 40;

							if (context == 'authUser') {
								var user = authUser;
							}
							else if (context == 'single') {
								var user = $scope.single;
							}
							else
							{
								var user = $scope[context].user;
							}
	
							img = '<img src="' + avatarUrl(user) + '" class="avatar" width="' + size + '">';
	
							$element.append('<a href="/@' + user.username + '">' + img + '</a>');
						}
					})
	
					avatarUrl = function(user) {
		
						if(user.avatar) {
							return '/img/avatars/' + user.username + '/' + user.avatar;
						}
						return '/img/avatars/default-' + user.gender + '.png';
					}
				};
	
			return {
				restrict: 'E',
				link: link
			};
		}];

	angular.module('elbseite').directive('avatar', avatarTemplate);

}());
(function () {

	var blockButton = function() {

		var blockButtonController = ['$scope', '$http', '$attrs', function($scope, $http, $attrs) {
		
					$scope.btnClass = $attrs.blocked ? 'active' : '';
		
					$scope.block = function() {
		
						$http.post('/api/user/' + $attrs.username + '/block')
							.success(function(data) {
								$scope.btnClass = data.blocked ? 'active' : '';
							});
					}
				}];

		return {
				template: '<div type="button" class="block-button" ng-class="btnClass" ng-click="block()"><div class="icon-denied-block"></div></div>',
				controller: blockButtonController
			};
	}

	angular.module('elbseite').directive('blockButton', blockButton);

}());